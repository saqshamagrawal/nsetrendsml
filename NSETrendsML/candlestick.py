from matplotlib import colors as mcolors
from matplotlib.collections import LineCollection, PolyCollection
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd  # to use pd.qcut for plot_bivariate_continuous
import random  # for plotting random signal

from six.moves import zip

from datetime import timedelta
from matplotlib.dates import date2num

def plot_candlestick(ax, stockdata):
    """Represent the open, close as a bar line and high low range as a
    vertical line.
    NOTE: this code assumes if any value open, low, high, close is
    missing they all are missing
    Parameters
    ----------
    ax : `Axes`
        an Axes instance to plot to
    opens : sequence
        sequence of opening values
    highs : sequence
        sequence of high values
    lows : sequence
        sequence of low values
    closes : sequence
        sequence of closing values
    width : int
        size of open and close ticks in points
    colorup : color
        the color of the lines where close >= open
    colordown : color
        the color of the lines where close <  open
    alpha : float
        bar transparency
    Returns
    -------
    ret : tuple
        (lineCollection, barCollection)
    """

    opens = stockdata['Open']
    highs = stockdata['High']
    lows = stockdata['Low']
    closes = stockdata['Close']
    colorup = 'xkcd:green'
    colordown = 'xkcd:light red'
    alpha = 0.75
    width = 0.6
    delta = width / 2.

    # Convert dates to numbers using "matplotlib dates"
    xaxisrange = mdates.date2num(stockdata.index)

    # Create open close bars
    barVerts = [((i - delta, open),
                 (i - delta, close),
                 (i + delta, close),
                 (i + delta, open))
                for i, open, close in zip(xaxisrange, opens, closes)
                if open != -1 and close != -1]

    # Create high low lines
    rangeSegments = [((i, low), (i, high))
                     for i, low, high in zip(xaxisrange, lows, highs)
                     if low != -1]

    # Add colors
    colorup = mcolors.to_rgba(colorup, alpha)
    colordown = mcolors.to_rgba(colordown, alpha)
    colord = {True: colorup, False: colordown}
    colors = [colord[open < close]
              for open, close in zip(opens, closes)
              if open != -1 and close != -1]

    useAA = 0,  # use tuple here
    lw = 0.5,   # and here
    rangeCollection = LineCollection(rangeSegments,
                                     colors=colors,
                                     linewidths=lw,
                                     antialiaseds=useAA,
                                     )

    barCollection = PolyCollection(barVerts,
                                   facecolors=colors,
                                   edgecolors=colors,
                                   antialiaseds=useAA,
                                   linewidths=lw,
                                   )

    # Set the corners of the plot
    minx, maxx = min(xaxisrange), max(xaxisrange)  # this kind of change to plot dates on x axis
    miny = min([low for low in lows if low != -1])
    maxy = max([high for high in highs if high != -1])
    corners = (minx, miny), (maxx, maxy)
    ax.update_datalim(corners)
    ax.autoscale_view()

    # add the bars and ranges
    ax.add_collection(rangeCollection)
    ax.add_collection(barCollection)

    # Setup the DateFormatter for the x axis
    date_format = mdates.DateFormatter('%d-%m-%y')
    ax.xaxis.set_major_formatter(date_format)

    return rangeCollection, barCollection

def plot_volume(ax, stockdata):

    volume = stockdata['Total Trade Quantity']
    opens = stockdata['Open']
    closes = stockdata['Close']
    colorup = 'xkcd:green'
    colordown = 'xkcd:light red'
    alpha = 0.75
    width = 0.6
    delta = width / 2.

    # Convert dates to numbers using "matplotlib dates"
    xaxisrange = mdates.date2num(stockdata.index)

    # Create bars
    barVerts = [((i - delta, 0),
                 (i - delta, vol),
                 (i + delta, vol),
                 (i + delta, 0))
                for i, vol in zip(xaxisrange, volume)
                if vol != -1]


    # Add colors
    colorup = mcolors.to_rgba(colorup, alpha)
    colordown = mcolors.to_rgba(colordown, alpha)
    colord = {True: colorup, False: colordown}
    colors = [colord[open < close]
              for open, close in zip(opens, closes)
              if open != -1 and close != -1]

    useAA = 0,  # use tuple here
    lw = 0.5,   # and here

    barCollection = PolyCollection(barVerts,
                                   facecolors=colors,
                                   edgecolors=colors,
                                   antialiaseds=useAA,
                                   linewidths=lw,
                                   )

    # Set the corners of the plot
    minx, maxx = min(xaxisrange), max(xaxisrange)  # this kind of change to plot dates on x axis
    # miny = min([low for low in lows if low != -1])
    # maxy = max([high for high in highs if high != -1])
    miny = min(volume)
    maxy = max(volume)
    corners = (minx, miny), (maxx, maxy)
    ax.update_datalim(corners)
    ax.autoscale_view()

    # add the bars and ranges
    # ax.add_collection(rangeCollection)
    ax.add_collection(barCollection)

    # Setup the DateFormatter for the x axis
    date_format = mdates.DateFormatter('%d-%m-%y')
    ax.xaxis.set_major_formatter(date_format)

    # return rangeCollection, barCollection

def plot_feature(ax, stockdata, featurename):

    feature = stockdata[featurename]
    opens = stockdata['Open']
    closes = stockdata['Close']
    colorup = 'xkcd:green'
    colordown = 'xkcd:light red'
    alpha = 0.75
    width = 0.6
    delta = width / 2.

    # Convert dates to numbers using "matplotlib dates"
    xaxisrange = mdates.date2num(stockdata.index)

    # Create bars
    barVerts = [((i - delta, 0),
                 (i - delta, featurevalue),
                 (i + delta, featurevalue),
                 (i + delta, 0))
                for i, featurevalue in zip(xaxisrange, feature)
                if featurevalue != -1]


    # Add colors
    colorup = mcolors.to_rgba(colorup, alpha)
    colordown = mcolors.to_rgba(colordown, alpha)
    colord = {True: colorup, False: colordown}
    colors = [colord[open < close]
              for open, close in zip(opens, closes)
              if open != -1 and close != -1]

    useAA = 0,  # use tuple here
    lw = 0.5,   # and here

    barCollection = PolyCollection(barVerts,
                                   facecolors=colors,
                                   edgecolors=colors,
                                   antialiaseds=useAA,
                                   linewidths=lw,
                                   )

    # Set the corners of the plot
    minx, maxx = min(xaxisrange), max(xaxisrange)  # this kind of change to plot dates on x axis
    # miny = min([low for low in lows if low != -1])
    # maxy = max([high for high in highs if high != -1])
    miny = min(feature)
    maxy = max(feature)
    corners = (minx, miny), (maxx, maxy)
    ax.update_datalim(corners)
    ax.autoscale_view()

    # add the bars and ranges
    # ax.add_collection(rangeCollection)
    ax.add_collection(barCollection)

    # Setup the DateFormatter for the x axis
    date_format = mdates.DateFormatter('%d-%m-%y')
    ax.xaxis.set_major_formatter(date_format)

    # return rangeCollection, barCollection


def plot_random_signal(subset, features, signalsdf):
    ticker, signal_date = random.choice(subset.index)  # this to be run on the subset representing signals
    stockdata = signalsdf.loc[ticker]  # but this to be run on a dataframe that has all dates, not a subset
    # signal = stockdata.loc[signal_date]
    signal = subset.loc[ticker].loc[signal_date]

    num_features = len(features)

    # Plot the stockdata
    fig, ax = plt.subplots(nrows=num_features+1, ncols=1, sharex='col')  # make it the same x axis on all
    plot_candlestick(ax[0], stockdata)
    # and the signal
    sell_date = signal_date + timedelta(days=signal.held_for_days)
    if signal.longshort == 'long':
        ax[0].plot([date2num(signal_date), date2num(sell_date)], [signal.buy_price, signal.sell_price], color='xkcd:blue', linewidth=2)
    if signal.longshort == 'short':
        ax[0].plot([date2num(signal_date), date2num(sell_date)], [-1 * signal.buy_price, -1 * signal.sell_price], color='xkcd:red', linewidth=2)

    # Zoom in on 60 days before buy and 60 days after sell
    plot_start_date = max(stockdata.index.min(), signal_date - timedelta(days=60))  # 60 days before signal date
    plot_end_date = min(stockdata.index.max(), sell_date + + timedelta(days=60))  # 60 days after sell date
    left, right = ax[0].get_xlim()  # return the current xlim
    new_left = max(left, date2num(plot_start_date))
    new_right = min(right, date2num(plot_end_date))
    ax[0].set_xlim(new_left, new_right)

    # Set axis limits
    max_price = max(stockdata.High.loc[plot_start_date:plot_end_date])
    min_price = min(stockdata.Low.loc[plot_start_date:plot_end_date])
    ax[0].set_ylim(min_price, max_price)

    # and the features
    counter = 0
    for feature in features:
        counter+=1
        plot_feature(ax[counter], stockdata, feature)

        max_feature = max(stockdata.loc[plot_start_date:plot_end_date][feature])
        min_feature = min(stockdata.loc[plot_start_date:plot_end_date][feature])
        ax[counter].set_ylim(min_feature, max_feature)

        ax[counter].set_ylabel(feature)  # set axis label


def plot_bivariate(signalsdf,var_list):
    for var_name in var_list:
        fig, ax = plt.subplots(nrows=1, ncols=1)
        samples = signalsdf.groupby([var_name]).winloss.count()  # count of total samples
        wins = signalsdf.groupby([var_name]).winloss.sum()  # count of winning samples
        bivariate_winloss = wins / samples  # win percent
        # Plot
        bivariate_winloss.plot(kind='bar', ax=ax)  # this does the actual plotting
        ax.set_ylim(0, 1)
        # Annotate with values
        for x, y in zip(samples.index, samples):
            ax.annotate("samples = "+str(y), xy=(x, 0.8), textcoords='data')
        for x, y in zip(wins.index, wins):
            ax.annotate("wins = "+str(y), xy=(x, 0.75), textcoords='data')
        for x, y in zip(bivariate_winloss.index, bivariate_winloss):
            y_in_perc = "{:.1%}".format(y)  # fix the formatting to get single-digit-percentage
            ax.annotate(y_in_perc, xy=(x, y + 0.03), textcoords='data')


def plot_bivariate_continuous(signalsdf,var_name,num_of_cuts):
    fig, ax = plt.subplots(nrows=1, ncols=1)
    varbins = pd.qcut(signalsdf[var_name], q=num_of_cuts)  # this one
    signalsdf.groupby(by=varbins).winloss.mean().plot(kind='line')
    print(signalsdf.groupby(by=varbins).winloss.mean())
    # does not print yet but can print

