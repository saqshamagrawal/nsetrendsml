import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')  # depends on project structure

import pickle
import datetime
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')  # must be called before importing pyplot
import matplotlib.pyplot as plt
import gspread  # for accessing portfolio and watchlist from Google Spreadsheet
from oauth2client.service_account import ServiceAccountCredentials  # for authenticating access to Google Spreadsheet
import pkg_resources  # for properly accessing client_secret
from datapull import pullNSEdatafromQuandl  # hosts the function where Quandl is accessed to pull data
from datapull import pullNSEdatafromAlphaVantage  # hosts the function where Alpha Vantage is accessed to pull data
from datapull import pullNSEdatafromSAMCO  # hosts the function where SAMCO is accessed to pull data
from technicals import gettechnicals
from features import getfeatures
import candlestick
import matplotlib.animation as anime
from matplotlib.ticker import PercentFormatter
import random  # this is used to randomize the text annotation on the graph
from sklearn.externals import joblib  # For loading the models # Deprecated - replace with installing joblib directly
from adjustText import adjust_text  # using https://github.com/Phlya/adjustText

# listing features by type
discrete_events = ['MACD12269_pos0cross', 'MACD12269_neg0cross', 'MACD12269_possignalcross', 'MACD12269_negsignalcross', 'MACD245218_pos0cross',
                   'MACD245218_neg0cross', 'MACD245218_possignalcross', 'MACD245218_negsignalcross', 'MACD6135_pos0cross', 'MACD6135_neg0cross',
                   'MACD6135_possignalcross', 'MACD6135_negsignalcross', 'MACD362_pos0cross', 'MACD362_neg0cross', 'MACD362_possignalcross',
                   'MACD362_negsignalcross', 'RSI_crossinto30', 'RSI_crossoutof30', 'RSI_crossinto70', 'RSI_crossoutof70', 'pricevolcorr_poscross',
                   'pricevolcorr_negcross', 'pricevolcorr_siginc', 'pricevolcorr_sigdec']
discrete_states = ['MACD12269_gt0', 'MACD12269_gtSignal', 'MACD245218_gt0', 'MACD245218_gtSignal', 'MACD6135_gt0', 'MACD6135_gtSignal', 'MACD362_gt0',
                   'MACD362_gtSignal', 'RSI_lt30', 'RSI_gt70', 'RSI_lt30_2weeksago', 'RSI_gt70_2weeksago', 'RSI_lt30_last7days', 'RSI_gt70_last7days',
                   'avgVol_rose_2weeksago', 'avgVol_rose_last7days', 'pricevolcorr_gt0', 'marketbreadthfast_gt0', 'marketbreadthslow_gt0',
                   'volatilityrose_2weeksago', 'volatilityrose_last7days', 'onedayreturn_gt0']
continuous = ['daily_volatility', 'pricevolcorr', 'avgVol_Volumevsavgvolume', 'avgVol_increase_last7days', 'pricevolcorr_delta', 'marketbreadthfast',
              'marketbreadthslow', 'volatilityincrease_last7days']

list_of_feature_lists = [discrete_events, discrete_states, continuous]

all_features = [item for sublist in list_of_feature_lists for item in sublist]

# Access the watchlist

scope = ['https://spreadsheets.google.com/feeds']
path = '/secret/client_secret.json'  # downloaded from Google API Console and saved at this location

CLIENT_SECRET_FILE = pkg_resources.resource_filename(__name__, path)
credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRET_FILE, scope)  # gives error unless chdir

gc = gspread.authorize(credentials)
daily_execution = gc.open_by_url('https://docs.google.com/spreadsheets/d/1d4RnsjtGMxEN3qWuhBTNdy4iWFl7Y_242uGaG5j7Zyg/')
# "Daily Execution" sheet, shared with "readportfoliodata@stockportfolionsetrends.iam.gserviceaccount.com"
watchlist = daily_execution.worksheet('Watchlist').get_all_records(empty2zero=False, head=1, default_blank='')
watchlist = pd.DataFrame(watchlist)  # makes it a pandas DataFrame

# watchlist_largecap = watchlist[watchlist['Category'] > 'Large']
# watchlist_midcap = watchlist[watchlist['Market Cap'] < 10000]

# Set parameters

testconfiguration = {
    'averaging_period': 14,  # for calculating technicals like RSI, and rolling means
    'lookback_period': 14,  # lookback period for getting features (aggregates) over these number of periods
    'history': 365,  # Calendar days, not trading days
    # The first few days (=averaging_period, but also few more in the lookback period) are not going to contribute
    'testname': datetime.datetime.now().strftime("test_%y_%m_%d"),
    'outputsize':100 #number of data points needed from datapull
}

watchlist = watchlist[:50]  # For nifty50

# NSEdata, datapull_time = pullNSEdatafromAlphaVantage(watchlist['Ticker'],outputsize=testconfiguration['outputsize'])  #outputsize-number of data points needed from datapull # pull full when doing backtesting/analysis
# saving data in pickl (testing)
# if os.path.exists('./dataset/t.pickle'):
#     with open('./dataset/t50.pickle', 'rb') as handle:
#         NSEdata = pickle.load(handle)
# else:
#     NSEdata, datapull_time = pullNSEdatafromAlphaVantage(watchlist['Ticker'],no_of_days = 100) #pulls data for only specified no of (trading)days  # pull compact when reducing mem size or prod
#     with open('./dataset/t.pickle', 'wb') as handle:
#         pickle.dump(NSEdata, handle, protocol=pickle.HIGHEST_PROTOCOL)


NSEdata = pullNSEdatafromSAMCO(watchlist['Ticker'],'2017-01-01')


print('Datapull complete')
# fig, ax = plt.subplots(nrows=1,ncols=1); candlestick.plot_candlestick(ax,NSEdata['INFY'][-100:])


NSEdatawithtechnicals = {}
for ticker, stockdata in NSEdata.items():
    print(ticker)
    if len(stockdata) > 0:
        NSEdatawithtechnicals[ticker] = gettechnicals(stockdata, 14)

print("Technical indicators added")

# Getting market breadth
# step 1 - initialize a dataframe of dates vs. tickers
advanced_declined_bool_fast = pd.DataFrame(index=NSEdatawithtechnicals['INFY'].index, columns=NSEdatawithtechnicals.keys())
advanced_declined_bool_slow = pd.DataFrame(index=NSEdatawithtechnicals['INFY'].index, columns=NSEdatawithtechnicals.keys())

# step 2 - populate each row with True and False for each day of advance vs. decline (uses df.update(Series.rename), sort of a left join
for ticker in NSEdatawithtechnicals.keys():
    # Define every ticker on every day as bullish or bearish
    # One day return is too "noisy", and to smoothen it, we use either MACD or RSI to define bullish or bearish
    # MACD provides various "timeframes" over which stock is bullish or bearish
    advanced_declined_bool_fast[ticker].update((NSEdatawithtechnicals[ticker]['MACD12269'] > NSEdatawithtechnicals[ticker]['MACD12269_signal']).rename(ticker, inplace=True))
    advanced_declined_bool_slow[ticker].update((NSEdatawithtechnicals[ticker]['MACD245218'] > NSEdatawithtechnicals[ticker]['MACD245218_signal']).rename(ticker, inplace=True))
    # RSI14 gives very similar breadth as MACD245218

declined_count_fast = (advanced_declined_bool_fast == False).sum(axis=1); advanced_count_fast = (advanced_declined_bool_fast == True).sum(axis=1)
declined_count_slow = (advanced_declined_bool_slow == False).sum(axis=1); advanced_count_slow = (advanced_declined_bool_slow == True).sum(axis=1)

# step 3 - define a method to combine advanced and declined counts to a usable metric
market_breadth_fast = (advanced_count_fast - declined_count_fast) / (advanced_count_fast + declined_count_fast)
market_breadth_slow = (advanced_count_slow - declined_count_slow) / (advanced_count_slow + declined_count_slow)


NSEdatawithfeatures = {}
for ticker, technicaldata in NSEdatawithtechnicals.items():
    print("features extracted for ticker"+ticker)
    NSEdatawithfeatures[ticker] = getfeatures(technicaldata,{},market_breadth_fast,market_breadth_slow,14)  # hardcoded averaging

print("Features extracted")

dailydf = pd.concat(NSEdatawithfeatures.values(),keys=NSEdatawithfeatures.keys(),sort=False)
dailydf.index.names = ['Ticker','Date']

# Loading the models
filename = '/models/randomforest.model'
ranfor_winloss = joblib.load(os.getcwd()+filename)

filename = '/models/regression.model'
reg_return = joblib.load(os.getcwd()+filename)

reg_features = ['MACD12269_negsignalcross', 'MACD245218_neg0cross', 'MACD6135_neg0cross', 'MACD362_pos0cross', 'MACD362_neg0cross',
                'RSI_crossoutof30', 'pricevolcorr_siginc', 'pricevolcorr_sigdec', 'MACD12269_gt0', 'MACD245218_gtSignal', 'MACD362_gt0',
                'RSI_lt30', 'RSI_gt70', 'RSI_lt30_last7days', 'RSI_gt70_last7days', 'marketbreadthfast_gt0', 'daily_volatility',
                'pricevolcorr_delta', 'marketbreadthfast', 'volatilityincrease_last7days']

# Scoring random forest
dailydf_scored = dailydf.replace([np.inf, -np.inf], np.nan)
dailydf_scored = dailydf_scored.dropna()
dailydf_scored['proba_rf'] = ranfor_winloss.predict_proba(dailydf_scored[all_features])[:, 1]
print("Random forest scored")

# Scoring linear regression
dailydf_scored['predicted_return'] = reg_return.predict(dailydf_scored[reg_features])
print("Regression scored")


# dailydf_scored = dailydf_scored.reset_index()

proba_rf_mean = dailydf_scored.groupby(by='Date').proba_rf.mean().to_frame(name='proba_rf_mean')
proba_rf_std = dailydf_scored.groupby(by='Date').proba_rf.std().to_frame(name='proba_rf_std')

dailydf_scored = dailydf_scored.reset_index().merge(proba_rf_mean,on='Date',how='left')
dailydf_scored = dailydf_scored.merge(proba_rf_std,on='Date',how='left')
# BEWARE - merge changes the index to just Date and loses the Ticker completely, so reset_index() beforehand
dailydf_scored['proba_rf_standardized'] = (dailydf_scored['proba_rf'] - dailydf_scored['proba_rf_mean']) / dailydf_scored['proba_rf_std']

dailydf_scored = dailydf_scored.set_index(['Ticker','Date'])

# To confirm that index works properly:
# dailydf_scored.loc['GRASIM'].loc[datetime.datetime(2019,5,27,0,0,0).date()]

dailydf_scored = dailydf_scored.sort_index()

# dailydf_scored['conviction_recent'] = 0
for ticker in dailydf_scored.index.levels[0]:
    print(ticker)
    rolling_mean = dailydf_scored.loc[ticker]['proba_rf_standardized'].rolling(5).mean()
    dailydf_scored.loc[(ticker,),'conviction_recent'] = rolling_mean.values
    #find market direction
    temp = dailydf_scored.loc[ticker]['marketbreadthfast'].diff().rolling(3).mean()
    #temp = dailydf_scored.loc[ticker]['marketbreadthfast'].diff()
    dailydf_scored.loc[(ticker,),'marketbreadthfast_diff'] = temp.values

dailydf_scored['CredM'] = -1*dailydf_scored['conviction_recent']
dailydf_scored['Vol'] = dailydf_scored['daily_volatility'] * np.sqrt(252) * 100  # Annualized the daily volatility


# Identifying short term direction
short_term_direction_conditions = [
    (dailydf_scored['marketbreadthfast'] > 0.7) & (dailydf_scored['marketbreadthfast'] <= 1),
    (dailydf_scored['marketbreadthfast'] >= 0) & (dailydf_scored['marketbreadthfast'] <= 0.7) & (dailydf_scored['marketbreadthfast_diff'] > 0),
    (dailydf_scored['marketbreadthfast'] >= 0) & (dailydf_scored['marketbreadthfast'] <= 0.7) & (dailydf_scored['marketbreadthfast_diff'] <= 0),
    (dailydf_scored['marketbreadthfast'] >= -0.7) & (dailydf_scored['marketbreadthfast'] <= -0.1) & (dailydf_scored['marketbreadthfast_diff'] > 0),
    (dailydf_scored['marketbreadthfast'] >= -0.7) & (dailydf_scored['marketbreadthfast'] <= -0.1) & (dailydf_scored['marketbreadthfast_diff'] <= 0),
    (dailydf_scored['marketbreadthfast'] >= -1) & (dailydf_scored['marketbreadthfast'] < -0.7),
    ]
'''
short_term_direction_conditions = [
    (dailydf_scored['marketbreadthfast'] > -0.1) & (dailydf_scored['marketbreadthfast'] < 0.1),
    (dailydf_scored['marketbreadthfast'] >= 0.1) & (dailydf_scored['marketbreadthfast'] <= 0.7),
    (dailydf_scored['marketbreadthfast'] > 0.7) & (dailydf_scored['marketbreadthfast'] <= 1),
    (dailydf_scored['marketbreadthfast'] >= -0.7) & (dailydf_scored['marketbreadthfast'] <= -0.1),
    (dailydf_scored['marketbreadthfast'] >= -1) & (dailydf_scored['marketbreadthfast'] < -0.7),
    ]
short_term_direction_names = ['balanced', 'bull trend', 'overbought', 'bear trend', 'oversold']
1. Bullish and going up
2. Bearish and going up
3. Bullish and going down
4. Bearish and going down
5. Oversold
6. Overbought
'''

short_term_direction_names = ['overbought', 'Bullish and going up', 'Bullish and going down', 'Bearish and going up','Bearish and going down', 'oversold']
dailydf_scored['short_term_direction'] = np.select(short_term_direction_conditions, short_term_direction_names, default=0)
print("Short term market direction identified")

# Remove the market breadth effect and get a range of conviction (roughly) between -3 and 2
# Standardized normal - subtract that days mean and divide by that days standard dev
# this should account for the sudden jump in proba_rf mean and std-dev when mb_slow falls below -0.75


''' -not using for visualization
for ticker in dailydf_scored.index.levels[0]:
    temp = dailydf_scored.loc[ticker]
    temp['cross_val'] = np.nan
    temp['cross_sign'] = np.nan
    crossover_levels = np.arange(-4.5,3.5,0.5)  # levels from -4.5 to +3
    for level_to_check in crossover_levels:
        # print(level_to_check)
        temp['CredM_gt_level'] = np.where(temp['CredM'] > level_to_check, 1, 0)  # temp var has whether or not CredM is gt level being checked
        temp['cross_val'] = np.where(temp['CredM_gt_level'].diff() != 0, level_to_check, temp['cross_val'])
        temp['cross_sign'] = np.where(temp['CredM_gt_level'].diff() != 0, temp['CredM_gt_level'].diff(), temp['cross_sign'])  # if diff is non-zero, then its vaue shows whether +ve or -ve cross
        # if +ve cross, then modify below for max(existing value, new value) for +ve cross, or min(existing value, new value) for -ve cross
        # temp['cross_val'] = np.where((temp['CredM_gt_level'].diff() != 0) & (temp['cross_sign'] == 1), level_to_check, temp['cross_val'])
        # temp['cross_val'] = np.where((temp['CredM_gt_level'].diff() != 0) & (temp['cross_sign'] == -1), level_to_check, temp['cross_val'])
    # print(temp[['cross_val','cross_sign']])
    # The only problem is if two levels are crossed on a single date, then only the bigger level is kept, because it rewrites during the loop
    # Assume for now that there's barely one crossover of a "0.5" level a day, not more
    dailydf_scored.loc[(ticker,),'cross_val'] = temp['cross_val'].values
    dailydf_scored.loc[(ticker,),'cross_sign'] = temp['cross_sign'].values
    dailydf_scored.loc[(ticker,),'CredM_change'] = temp['CredM'].diff()
    print("Crossovers identified for "+ticker)

'''

# Pickling
dailydf_scored.to_pickle('latest_scored.pickle')
print('Saved to pickle')

# # Unpickling
#dailydf_scored = pd.read_pickle('latest_scored.pickle')
# # to get back to NSEdata et. al (dict of dfs) from unpickled signalsdf
# NSEdatawithfeatures = {}
# for ticker in dailydf_scored.index.levels[0]:
#     NSEdatawithfeatures[ticker] = dailydf_scored.loc[ticker]
#





# Till pickling and unpickling







# Plotting latest with one year history
date_of_interest = dailydf_scored.loc['INFY'].iloc[-1].name  # latest date available in scored data
# date_of_interest = datetime.datetime(2019, 4, 5, 0, 0)  # old date to test
one_day_all = dailydf_scored.iloc[dailydf_scored.index.get_level_values('Date') == date_of_interest]
one_day_all = one_day_all.reset_index().set_index(['Ticker'])
#previous date to find Credm change
previous_day_date = dailydf_scored.loc['INFY'].iloc[-2].name
previous_day_all = dailydf_scored.iloc[dailydf_scored.index.get_level_values('Date') == previous_day_date]
previous_day_all = previous_day_all.reset_index().set_index(['Ticker'])

#change in Credm
one_day_all['CredM_change'] = previous_day_all['CredM'] - one_day_all['CredM']

one_year_ago = date_of_interest - datetime.timedelta(days = 365)
one_year_history = dailydf_scored.iloc[(dailydf_scored.index.get_level_values('Date') >= one_year_ago) & (dailydf_scored.index.get_level_values('Date') <= date_of_interest)]


print(one_day_all[['CredM']].sort_values(by='CredM',ascending=False))
# CredM interpretation - <-2 is oversold, >1.5 is overbought, if market is in bull trend, then [1,2] are leading the bull trend

# Since market is in bull trend (at least with fast, if not slow), therefore:

print('Oversold stocks')
print(one_day_all[one_day_all.CredM < -2][['CredM']].sort_values(by='CredM',ascending=True))

print('Overbought stocks')
print(one_day_all[one_day_all.CredM > 1.5][['CredM']].sort_values(by='CredM',ascending=False))

print('Strong stocks in a bull trend')
print(one_day_all[(one_day_all.CredM > +0.5) & (one_day_all.CredM < 1.5)][['CredM']].sort_values(by='CredM',ascending=False))

'''
# Printing Companies that moved from one bucket to another (modifying this can create a daily commentary about bullish and bearish crossovers
sign_dict = {-1:'negatively',1:'positively'}
crossover_levels = np.arange(-4.5, 3.5, 0.5)  # levels from -4.5 to +3
for sign in [-1,1]:
    for level_to_check in crossover_levels:
        crossover = one_day_all[(one_day_all.cross_val == level_to_check) & (one_day_all.cross_sign == sign)].reset_index()
        for ticker in crossover.Ticker:
            print(ticker+" crossed CredM level "+str(level_to_check)+" "+sign_dict[sign])

'''



# Generating daily graph

fig, (ax_marketbreadth, ax_probs) = plt.subplots(nrows=2, ncols=1, gridspec_kw={'height_ratios': [1, 4]})
fig.set_size_inches(fig.get_size_inches() * 2.5)

# Market Breadth
ax_marketbreadth.set_xlim(one_year_history.index.min()[1], one_year_history.index.max()[1])
ax_marketbreadth.set_ylim(-1, 1)
ax_marketbreadth.axhline(y=0, color='black', linewidth=0.5)
# ax_marketbreadth.plot(one_year_history.loc['INFY'].marketbreadthslow,color='xkcd:aquamarine')
ax_marketbreadth.plot(one_year_history.loc['INFY'].marketbreadthfast,color='xkcd:burnt orange')  # kept fast mb because its early warning
ax_marketbreadth.grid()
ax_marketbreadth.set_title(label='Market Breadth')
plt.setp(ax_marketbreadth.get_xticklabels(), rotation=30, horizontalalignment='right')
# Daily CredM Plot
fig.subplots_adjust(hspace=0.3)

ax_probs.clear()
ax_probs.set_title(label='Daily Momentum Indicator ')

# y-axis as a log scale (for Vol)
ax_probs.set_yscale('log')  # log scale looks better but seems to have a bug (https://github.com/matplotlib/matplotlib/issues/6915)
# y-axis ticks and labels

# y-axis limits
# top = max(one_day_all.Vol) + 3  # this was for linear scale of Vol
# bottom = min(one_day_all.Vol) - 1.5
top = max(one_day_all.Vol) + (10 - max(one_day_all.Vol) % 10) + 10
if (min(one_day_all.Vol)//1) > 10:
    bottom = 10
else:
    bottom = min(one_day_all.Vol)//1

#bottom = min(one_day_all.Vol)//1
ax_probs.set_ylim(bottom, top)  # to give some space outside the limits

# y-axis ticks and labels
ax_probs.set_yticks(range(10,int(top),10))  # [10,20,30,40...] till the max
ax_probs.set_yticklabels(range(10,int(top),10))

# x-axis limits
# left_lim = -4
crossover_levels = np.arange(-4.5, 3.5, 0.5)
left_lim = min(max([t for t in crossover_levels if t < min(one_day_all.CredM.values)]),-2.5)  # min is either -2.5 or lower if there's a value
# right_lim = +3
right_lim = max(min([t for t in crossover_levels if t > max(one_day_all.CredM.values)]),2)  # max is either 2 or higher if there's a value
ax_probs.set_xlim(left_lim, right_lim)

# x-axis ticks and labels
ax_probs.set_xticklabels([])
# removing ticks
ax_probs.tick_params(axis='both',bottom=False, left=False)

# Fill colours by levels
crossover_levels = np.arange(-4.5,3.5,0.5)  # levels from -4.5 to +3
# Rough copy of RdYlGn but not scientifically placed, overbought above 1.5, and oversold below -2
# level_colors = ['#0A572C','#0A572C','#0A572C','#0A572C','#1D893E',
#                 '#EF5637','#F9A04B','#FADA7A','#FFFEB1','#D2F074','#98D35C','#50B651',
#                 '#C91B1D','#8D031D','#8D031D','None']  # None is needed just to simplify the loop

level_colors = matplotlib.cm.RdYlGn(np.linspace(0,1, 16))
#using https://stackoverflow.com/questions/25408393/getting-individual-colors-from-a-color-map-in-matplotlib
i=0
for level in crossover_levels:
    ax_probs.fill_betweenx(y=[bottom, top], x1=[level], x2=[level+0.5], where=None, facecolor=level_colors[i], alpha=0.7)
    i += 1

# Plotting scatter plot of each day

# Setting color for single scatter plot
# color= ['red' if sign == -1 else 'green' if sign == 1 else 'xkcd:light blue' for sign in one_day_all.cross_sign]
# scatter = ax_probs.scatter(one_day_all.CredM, one_day_all.Vol, color=color)  # one
'''
# For better control, three different scatters
scatter_neg = ax_probs.scatter(one_day_all[one_day_all.cross_sign == -1].CredM, one_day_all[one_day_all.cross_sign == -1].Vol, color='red', marker = "<")
scatter_pos = ax_probs.scatter(one_day_all[one_day_all.cross_sign == 1].CredM, one_day_all[one_day_all.cross_sign == 1].Vol, color='green', marker = ">")
scatter_nan = ax_probs.scatter(one_day_all[np.isnan(one_day_all.cross_sign)].CredM, one_day_all[np.isnan(one_day_all.cross_sign)].Vol, color='xkcd:pastel blue', marker = "o")
'''
#using credM change instead of crossover
scatter_neg = ax_probs.scatter(one_day_all[one_day_all.CredM_change <= -0.3].CredM, one_day_all[one_day_all.CredM_change <= -0.3].Vol, color='red', marker = "<")
scatter_pos = ax_probs.scatter(one_day_all[one_day_all.CredM_change >= 0.3].CredM, one_day_all[one_day_all.CredM_change >= 0.3].Vol, color='green', marker = ">")
scatter_nan = ax_probs.scatter(one_day_all[(one_day_all.CredM_change >= -0.3) & (one_day_all.CredM_change <= 0.3)].CredM, one_day_all[(one_day_all.CredM_change >= -0.3) & (one_day_all.CredM_change <= 0.3)].Vol, color='xkcd:pastel blue', marker = "o")

# Annotating with ticker names
# to prevent overlap of text labels, we just randomize where it is place wrt the point (helps but only a little)

#for k, txt in enumerate(one_day_all.reset_index().Ticker):
#    text_xcoord = one_day_all.CredM[k] + 0.015
#    text_ycoord = one_day_all.Vol[k]+ ((random.randint(0,1) - 0.5) * 0.02 * random.randint(0,100))
#    ax_probs.annotate(txt, xy=(one_day_all.CredM[k], one_day_all.Vol[k]), xycoords = 'data', xytext=(text_xcoord, text_ycoord), fontsize=7)

texts = []
for k, txt in enumerate(one_day_all.reset_index().Ticker):
    x,y = one_day_all.CredM[k], one_day_all.Vol[k]
    texts.append(plt.text(x, y, txt,fontsize=7))

adjust_text(texts)
# ax_probs.grid()  # for adding or removing grid

# Create 2X2 borders / or grid
ax_probs.axvline(x=-2, color='black', linewidth=2)
ax_probs.axvline(x=1.5, color='black', linewidth=2)
ax_probs.axhline(y=40, color='black', linewidth=0.2)
ax_probs.axhline(y=20, color='black', linewidth=0.2)

ax_probs.text(left_lim + 0.04, top + (top*0.072), "As of market close on: " + date_of_interest.strftime("%A, %B %e, %Y"), horizontalalignment='left', verticalalignment='top')
ax_probs.text(left_lim + 0.04, top - (top*0.02), "market direction:   " + one_day_all.short_term_direction[0], horizontalalignment='left', verticalalignment='top')

# Axis labels
ax_probs.set_ylabel('Volatility')

x_label_y_placement = bottom - 0.1*bottom
# Add x axis text (depending on marketbreadthfast (need to confirm each of these using backtesting)
ax_probs.text(-0.25, x_label_y_placement, '<- in line with market ->', horizontalalignment='center', verticalalignment='bottom')
ax_probs.text(1.75, x_label_y_placement, 'overbought', horizontalalignment='center', verticalalignment='bottom')
ax_probs.text(-2.25, x_label_y_placement, 'oversold', horizontalalignment='center', verticalalignment='bottom')

tags = {
        'Bullish and going up':{
            'left':'<-- lagging bulls',
            'right':'leading bulls -->'
            },
        'Bullish and going down':{
                'left':'<-- lagging bulls',
                'right':'leading bulls -->'
                },
        'Bearish and going down':{
                'left':'<-- leading bear',
                'right':'lagging bear -->'
                },
        'Bearish and going up':{
                'left':'<-- leading bear',
                'right':'lagging bear -->'
                },
        'overbought':{
                'left':'<-- lagging bulls',
                'right':'leading bulls / overbought -->'
                },
        'oversold':{
                'left':'<-- oversold / leading bears',
                'right':'lagging bears -->'
                }
    }

ax_probs.text(0.75, x_label_y_placement, tags[one_day_all.short_term_direction[0]]['right'], horizontalalignment='center', verticalalignment='bottom')
ax_probs.text(-1.25, x_label_y_placement,tags[one_day_all.short_term_direction[0]]['left'], horizontalalignment='center', verticalalignment='bottom')
# Legend

ax_probs.legend((scatter_neg,scatter_pos,scatter_nan),('Negative','Positive','None'),
                loc='upper right', fancybox=True, title="CredM Change", framealpha=0, handletextpad=0.1)

print("Generated daily chart")


filename = os.path.join('output', 'CredM_daily_chart.png')
fig.savefig(filename, bbox_inches='tight')


# Till here it can be automated

''' Not focusing here for Now
# Graph of a single stock (generates 50 graphs!)

from matplotlib.ticker import NullFormatter # for log axis of volatility

for ticker, date in one_day_all.index:
    # ticker = 'INFY'  # replace with loop
    # plotting a single one
    stockdata = one_year_history.loc[ticker]
    fig, ax = plt.subplots(nrows=3, ncols=1, sharex='col', gridspec_kw={'height_ratios': [2, 1, 2]})  # make it the same x axis on all
    fig.set_size_inches(fig.get_size_inches() * 2.5)

    fig.suptitle(ticker,fontsize=10)
    ax[0].set_title(label='Daily Price')
    from candlestick import plot_candlestick, plot_feature
    plot_candlestick(ax[0], stockdata)
    ax[0].tick_params(labelright=True)
    # Add gridlines or levels etc.

    ax[1].set_title(label='Historical Volatility')
    ax[1].plot(stockdata['Vol'])
    ax[1].set_yscale('log')
    ax[1].yaxis.set_major_formatter(NullFormatter())
    ax[1].yaxis.set_minor_formatter(NullFormatter())
    top_vol_ticker = max(stockdata.Vol) + (10 - max(stockdata.Vol) % 10) + 10
    # y-axis ticks and labels
    ax[1].set_yticks(range(10,int(top_vol_ticker),10))  # [10,20,30,40...] till the max
    ax[1].set_yticklabels(range(10,int(top),10))
    ax[1].axhline(y=40, color='black', linewidth=0.2)
    ax[1].axhline(y=20, color='black', linewidth=0.2)
    ax[1].set_ylabel('Volatility')

    ax[2].set_title(label='Daily Momentum Indicator')
    ax[2].plot(stockdata['CredM'])
    # Add colours similar to main graph
    top_credm_ticker, bottom_credm_ticker = ax[2].get_ylim()  # store the current ylim
    left, right = ax[2].get_xlim()  # return the current xlim
    i=0
    for level in crossover_levels:
        ax[2].fill_between(x=[left, right], y1=[level], y2=[level+0.5], where=None, facecolor=level_colors[i], alpha=0.2)
        i += 1
    ax[2].set_ylim(top_credm_ticker, bottom_credm_ticker) # reset to previous y-axis limits

    ax[2].set_yticklabels([])
    ax[2].tick_params(axis='y', left=False)

    # Save
    filename = os.path.join('output', ticker+'.png')
    fig.savefig(filename, bbox_inches='tight')

# Notes:
# Can plot both Vol and CredM on same x axis below the price
# ax2y = ax[2].twinx()  # instantiate a second axes that shares the same x-axis



# ------------------- Current End Marker of actual "daily" code ---------


##################### ANALYSIS 1 - HEATMAPS  ####################################################################

# Analysis (e.g. heatmaps) requiring scoring the dataset with historical actual returns

# Getting probs for a given period
start = datetime.datetime(2018, 1, 1, 0, 0)
stop = datetime.datetime(2018, 12, 31, 0, 0)
score_2018 = dailydf_scored.iloc[dailydf_scored.index.get_level_values('Date') >= start]
score_2018 = score_2018.iloc[score_2018.index.get_level_values('Date') <= stop]

score_2015_MAX5 = score_2018.copy(deep=True)
score_2017_MAX10 = score_2018.copy(deep=True)

# get returns
from functions import scoreExit, combine, printPerformance
performance_vars = ['buy_price','sell_price','signal_return','held_for_days','winloss','winloss2']
score_2018['longshort'] = 'long'
score_2018[performance_vars] = score_2018.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MIN0','static2','static4'),axis=1)
printPerformance(score_2018)

# View crossovers performance

for level in crossover_levels:
    for sign in [-1,1]:
        print(level, sign, "mb+")
        printPerformance(score_2018[(score_2018.cross_sign == sign) & (score_2018.cross_val == level) & (score_2018.marketbreadthfast < 0)])

# Create Heatmaps

a = score_2018[['CredM','marketbreadthfast','proba_rf', 'predicted_return', 'daily_volatility', 'winloss','signal_return']].reset_index()

# First create a grid
dx, dy = 0.5, 0.225  # how granular do we want to see each var
x, y = np.mgrid[slice(-3, 2 + dx, dx), slice(-1, 1 + dy, dy)]
# x, y = np.mgrid[slice(0, 1.5 + dx, dx), slice(-1, 1 + dy, dy)]  # to zero in on one part of the grid
# x and y have the same shape. check it, because -1, -1 is the shape of z (z should be  np.ndarray of shape [15, 20] if x.shape is (16, 21)))
np.shape(x)
x_axis_len = np.shape(x)[0] - 1
y_axis_len = np.shape(x)[1] - 1

# writing a z which is the value to be plotted as color
# getting z's for - buckets of CredM (x) and marketbreadthfast (y) - with winloss.mean() as z (range(.) should contain -1 the shape)

z = np.zeros((x_axis_len,y_axis_len))

# plotting the z as color
fig, ax = plt.subplots(nrows=3,ncols=1)

# For mean of winloss
for i in range(x_axis_len):
    for j in range(y_axis_len):
        if i == 0:
            if j == 0:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
            elif j == y_axis_len - 1:  # this is to fix the edges and count everything beyond that edge
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j])].winloss.mean()
            else:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
        elif i == x_axis_len - 1:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j])].winloss.mean()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
        else:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j])].winloss.mean()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].winloss.mean()
        # print(z[i][j])
c = ax[0].pcolor(x, y, z, cmap='RdYlGn')
plt.colorbar(c, ax=ax[0])
ax[0].set_title(label='win rate')

# For mean of signal return
for i in range(x_axis_len):
    for j in range(y_axis_len):
        if i == 0:
            if j == 0:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
            elif j == y_axis_len - 1:  # this is to fix the edges and count everything beyond that edge
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j])].signal_return.mean()
            else:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
        elif i == x_axis_len - 1:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j])].signal_return.mean()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
        else:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j])].signal_return.mean()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.mean()
        # print(z[i][j])
c = ax[1].pcolor(x, y, z, cmap='RdYlGn')
plt.colorbar(c, ax=ax[1])
ax[1].set_title(label='avg return')

# For count of signals
for i in range(x_axis_len):
    for j in range(y_axis_len):
        if i == 0:
            if j == 0:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
            elif j == y_axis_len - 1:  # this is to fix the edges and count everything beyond that edge
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j])].signal_return.count()
            else:
                z[i][j] = a[(a['CredM'] < x[i + 1][j + 1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
        elif i == x_axis_len - 1:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j])].signal_return.count()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
        else:
            if j == 0:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
            elif j == y_axis_len - 1:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j])].signal_return.count()
            else:
                z[i][j] = a[(a['CredM'] > x[i][j]) & (a['CredM'] < x[i+1][j+1]) & (a['marketbreadthfast'] > y[i][j]) & (a['marketbreadthfast'] < y[i + 1][j + 1])].signal_return.count()
        # print(z[i][j])
c = ax[2].pcolor(x, y, z, cmap='RdYlGn')
plt.colorbar(c, ax=ax[2])
ax[2].set_title(label='no. of signals')

# cmap='YlGn' or 'plasma' or 'viridis_r'

# next steps

# 1. Use above heatmap approach to create interpretations of CredM and marketbreadthfast / marketbreadthslow
# Found some relationships that exist across years - in particular, confirms that market breadth has a role to play in interpreting CredM

# 2. Confirm the interpretation using backtesting of strategies
# 3. Confirmation will come if the results stand true for Max5, max10, CredM based exits, for score_2018, score_2017, score_2013 etc.
# a) output should be a set of backtest results - Success ratio, P&L, VaR, Sharpe
# 4. based on above, create stable daily output - one png, one txt, and maybe 50 other pngs in a folder
# a)
# 5. Create separate code base for this - just like we did for nse_trends. Call it credm_prod. it should put stable daily output into a
# 5. Read this folder contents from website / webpage
# 6. Do digital outreach


##################### ANALYSIS 2 - PRICING RELATED  ####################################################################

# For options price estimation


# Redoing the support and resistance levels with new funda
stockdata = one_year_history.loc['ITC']
print(stockdata.proba_rf[-1])
print(stockdata.predicted_return[-1])
current_price = stockdata.Close[-1]
print('Current price '+str(current_price))
daily_vol = stockdata.daily_volatility[-1]
daily_vol * current_price
# For 95% confidence interval
lowpred4 = current_price * (1 - 2*np.sqrt(2)*daily_vol) # in absence of trend, in 2 days how low can it go with a 95% confidence interval
print('4-day/2-SD possible low '+str(lowpred4))
highpred8 = current_price * (1 + 2*np.sqrt(8)*daily_vol)  # in absence of trend, in 8 days how high can it go with a 95% confidence interval
print('8-day/2-SD possible high '+str(highpred8))

print('Last 20-day actual low '+str(stockdata[-20:].Low.min()))
print('Last 20-day actual high '+str(stockdata[-20:].High.max()))

print('Last 14-day actual low '+str(stockdata[-14:].Low.min()))
print('Last 14-day actual high '+str(stockdata[-14:].High.max()))

print('14-day/2-SD Bollinger low '+str(stockdata.Bollinger_Low[-1]))
print('14-day/2-SD Bollinger high '+str(stockdata.Bollinger_High[-1]))


# For Option strike price - Below is wrong but get a sense of the probability of reaching a certain price by a certain date

from dateutil.relativedelta import relativedelta, TH
last_thu_of_month = date_of_interest + relativedelta(day=31, weekday=TH(-1))
last_thu_of_next_month = date_of_interest + relativedelta(day=31, month=1, weekday=TH(-1))

a = (last_thu_of_month - date_of_interest).days

prob_price_expiry_68 = one_day_all.loc['TATAMOTORS'].Close.values * (1+one_day_all.loc['TATAMOTORS'].daily_volatility.values * np.sqrt(a))

# For instance getting the strike with delta 0.3 = strike price for which the probability to reach by expiry is 30%


##################### ANALYSIS 3 - OUTDATED VISUALIZATIONS  ####################################################################

# Spent quite a few days during April and May based on these below, before developing CredM

# Just to remember that plotting animation can be done (needs an "animate all" function similar to the "plot_date_of_interest" function
# num_frames = len(period_all.reset_index(level='Ticker').index.unique())
# anime.FuncAnimation(fig,animate_all,frames=num_frames,init_func=plot_date_of_interest(date_of_interest,period_all),repeat=True,interval=1000)

# this old method of visualizing proba_rf and predicted_return still works
# But from here, we moved to conviction, to conviction_recent (a rolling mean) and finally the Cred Momentum Indicator
# below works but not a well-written function since it uses global vars one_day_all
def plot_date_of_interest(date_of_interest,period_all):
    # Plotting market breadth of recent history
    ax_marketbreadth.clear()
    ax_marketbreadth.set_xlim(period_all.index.min()[1], period_all.index.max()[1])
    ax_marketbreadth.set_ylim(-1, 1)
    ax_marketbreadth.axhline(y=0, color='black', linewidth=0.5)
    ax_marketbreadth.plot(period_all.loc['INFY'].marketbreadthslow,color='xkcd:aquamarine')
    ax_marketbreadth.plot(period_all.loc['INFY'].marketbreadthfast,color='xkcd:burnt orange')

    # Setting up the plot
    ax_probs.clear()
    top = min(0.015, one_day_all.predicted_return.max())
    bottom = max(-0.005, one_day_all.predicted_return.min())
    left_lim = one_day_all.proba_rf.min()
    right_lim = one_day_all.proba_rf.max()

    ax_probs.set_ylim(bottom - 0.0005, top + 0.0001)
    ax_probs.set_xlim(left_lim - 0.001, right_lim + 0.001)

    # Plotting background colours (a little adhoc)
    center = 0.545 - 0.02 * one_day_all.marketbreadthfast[0]  # when market is oversold, the center is adjusted to the right
    left = center - 0.01
    leftmost = center - 0.025
    right = center + 0.01
    rightmost = center + 0.025

    ax_probs.fill_betweenx(y=[bottom, top], x1=[0.5], x2=[leftmost], where=None, facecolor='xkcd:rose', alpha=0.5)
    ax_probs.fill_betweenx(y=[bottom, top], x1=[leftmost], x2=[left], where=None, facecolor='xkcd:light pink', alpha=0.5)
    ax_probs.fill_betweenx(y=[bottom, top], x1=[left], x2=[right], where=None, facecolor='xkcd:light grey', alpha=0.5)
    ax_probs.fill_betweenx(y=[bottom, top], x1=[right], x2=[rightmost], where=None, facecolor='xkcd:very light green', alpha=0.5)
    ax_probs.fill_betweenx(y=[bottom, top], x1=[rightmost], x2=[0.6], where=None, facecolor='xkcd:teal green', alpha=0.5)

    # Plotting scatter plot of each day

    # ax_probs.hlines(y=0.008, xmin=0.57, xmax=0.58, linestyles='dashed', linewidth=0.5)
    # ax_probs.hlines(y=-0.002, xmin=0.5, xmax=0.53, linestyles='dashed', linewidth=0.5)
    # ax_probs.hlines(y=0.012, xmin=0.53, xmax=0.57, linestyles='dashed', linewidth=0.5)
    # ax_probs.hlines(y=0, xmin=0.53, xmax=0.57, linestyles='dashed', linewidth=0.5)
    # ax_probs.vlines(x=0.53, ymin=-0.005, ymax=0.015, linestyles='dashed', linewidth=0.5)
    # ax_probs.vlines(x=0.57, ymin=-0.005, ymax=0.015, linestyles='dashed', linewidth=0.5)
    a = one_day_all[['proba_rf', 'predicted_return','daily_volatility']].reset_index()
    mbtxt = 'market breadth (slow): ' + format(one_day_all.marketbreadthslow[0], '.2f') + ' | market breadth (fast): ' + format(one_day_all.marketbreadthfast[0], '.2f')
    plt.scatter(a.proba_rf, a.predicted_return, color='xkcd:slate')
    # plt.scatter(a.proba_rf, a.predicted_return, c=a.daily_volatility, cmap='YlGn')  # or 'plasma' or 'viridis_r'
    # cb = plt.colorbar()
    for k, txt in enumerate(a.reset_index().Ticker):
        ax_probs.annotate(txt, (a.proba_rf[k], a.predicted_return[k]), fontsize=8, textcoords='offset points')
    ax_probs.text(left_lim, top, date_of_interest.strftime("%d/%m/%Y"), horizontalalignment='left', verticalalignment='top')
    ax_probs.text(left_lim, top - 0.001, mbtxt, horizontalalignment='left', verticalalignment='top')
    ax_probs.text(left_lim, top - 0.002, one_day_all.short_term_direction[0], horizontalalignment='left', verticalalignment='top')
    ax_probs.text(center, bottom - 0.0005, 'unclear / sideways', horizontalalignment='center', verticalalignment='bottom', bbox=dict(facecolor='white', alpha=1, edgecolor=None))
    ax_probs.text(rightmost+0.003, bottom - 0.0005, 'oversold', horizontalalignment='center', verticalalignment='bottom', bbox=dict(facecolor='white', alpha=1, edgecolor=None))
    ax_probs.text(right+0.007, bottom - 0.0005, 'leading the downtrend / lagging the uptrend', horizontalalignment='center', verticalalignment='bottom', bbox=dict(facecolor='white', alpha=1, edgecolor=None))
    ax_probs.text(left-0.007, bottom - 0.0005, 'lagging the downtrend / leading the uptrend', horizontalalignment='center', verticalalignment='bottom', bbox=dict(facecolor='white', alpha=1, edgecolor=None))
    ax_probs.text(leftmost-0.003, bottom - 0.0005, 'overbought', horizontalalignment='center', verticalalignment='bottom', bbox=dict(facecolor='white', alpha=1, edgecolor=None))

fig, (ax_marketbreadth,ax_probs) = plt.subplots(nrows=2,ncols=1,gridspec_kw = {'height_ratios':[1, 3]})
plot_date_of_interest(date_of_interest,one_year_history)

# Printing by segments
one_day_all.groupby(by='segment')['Close'].count()
# Printing by decreasing probabilities
one_day_all[['proba_rf', 'predicted_return']].sort_values(by='proba_rf',ascending=False)
one_day_all[['proba_rf', 'predicted_return']].sort_values(by='predicted_return',ascending=False)


##################### ANALYSIS 4 - STOCK-SPECIFIC  ####################################################################

# The below is to confirm that
# CredM levels are actually different for different stocks - some stocks lead the market more often than others
# and some stocks move in a wider range around the average than others

# For instance, the histogram of CredM is a "signature" of a particular HRITHIK stock

crossover_levels = np.arange(-3,3.5,0.5)
fig = plt.figure()
tickerlist = ['HDFC','RELIANCE','INFY','TCS','HINDUNILVR','INDUSINDBK','KOTAKBANK']
for i in range(1,8):
    ticker=tickerlist[i-1]
    axis = fig.add_subplot(7, 1, i)
    axis.hist(one_year_history.loc[ticker].CredM,crossover_levels,weights=np.ones(len(one_year_history.loc[ticker])) / len(one_year_history.loc[ticker]))
    axis.yaxis.set_major_formatter(PercentFormatter(1))

# while histograms of RSI are pretty much similar
RSI_bins = np.arange(20,80,10)
fig = plt.figure()
tickerlist = ['HDFC','RELIANCE','INFY','TCS','HINDUNILVR','INDUSINDBK','KOTAKBANK']
for i in range(1,8):
    ticker=tickerlist[i-1]
    axis = fig.add_subplot(7, 1, i)
    axis.hist(one_year_history.loc[ticker].RSI,RSI_bins,weights=np.ones(len(one_year_history.loc[ticker])) / len(one_year_history.loc[ticker]))
    axis.yaxis.set_major_formatter(PercentFormatter(1))


'''