import numpy as np
import datetime

def getfeatures(stockdatawithtechnicals,nifty_data,market_breadth_fast,market_breadth_slow,lookback_period):

    # Essentially, various aggregations of several time series over the last lookback_period

    # generic = On <day>, <ticker> had <indicator> <movement or aggregate function, min, max, decreased, increased, more than, less than, > <last X days>
    # Example:
    # Has the MACD crossed over in the last 3 days?
    # How much has the volume increased in the last 3 days?
    # Was RSI too low recently (<30 in last 7 days)?

    features = stockdatawithtechnicals  # same dataframe to be returned with additional columns

    # Features using MACD

    features['MACD12269_gt0'] = np.where(features['MACD12269'] > 0,1,0)
    features['MACD12269_gtSignal'] = np.where(features['MACD12269_hist'] > 0,1,0)
    with np.errstate(invalid='ignore'):
        features['MACD12269_pos0cross'] = np.maximum(features['MACD12269_gt0'].diff(), 0)  # sort of like taking a differential
        features['MACD12269_neg0cross'] = -1 * np.minimum(features['MACD12269_gt0'].diff(), 0)  # because .diff subtracts value - previous row value
        features['MACD12269_possignalcross'] = np.maximum(features['MACD12269_gtSignal'].diff(), 0)  # so takes values of 1 and -1 in current case
        features['MACD12269_negsignalcross'] = -1 * np.minimum(features['MACD12269_gtSignal'].diff(), 0)  # which max/min, converts back to only (1,0) or (-1,0)

    # to get time-shifted features - although, with 4 different MACDs, prolly dont need time shifted vars
    # features['MACD12269_pos0cross_last3days'] = np.maximum(features['MACD12269_gt0'].diff(3), 0)  # sort of like taking a differential with a length > 1
    # features['MACD12269_pos0cross_last7days'] = np.maximum(features['MACD12269_gt0'].diff(7), 0)
    # features['MACD12269_pos0cross_2weeksago'] = np.maximum(features['MACD12269_gt0'].shift(7).diff(3), 0)  # can be expanded to have 1 for a whole week

    # features['MACD12269_poszerocross'] = np.where(features['MACD12269_gt0'] > features['MACD12269_gt0'].shift(1),1,0)
    # features['MACD12269_negzerocross'] = np.where(features['MACD12269_gt0'] < features['MACD12269_gt0'].shift(1),1,0)
    # features['MACD12269_possignalcross'] = np.where(features['MACD12269_gtSignal'] > features['MACD12269_gtSignal'].shift(1),1,0)
    # features['MACD12269_negsignalcross'] = np.where(features['MACD12269_gtSignal'] < features['MACD12269_gtSignal'].shift(1),1,0)

    features['MACD245218_gt0'] = np.where(features['MACD245218'] > 0,1,0)
    features['MACD245218_gtSignal'] = np.where(features['MACD245218_hist'] > 0,1,0)
    with np.errstate(invalid='ignore'):
        features['MACD245218_pos0cross'] = np.maximum(features['MACD245218_gt0'].diff(), 0)  # sort of like taking a differential
        features['MACD245218_neg0cross'] = -1 * np.minimum(features['MACD245218_gt0'].diff(), 0)  # because .diff subtracts value - previous row value
        features['MACD245218_possignalcross'] = np.maximum(features['MACD245218_gtSignal'].diff(), 0)  # so takes values of 1 and -1 in current case
        features['MACD245218_negsignalcross'] = -1 * np.minimum(features['MACD245218_gtSignal'].diff(), 0)  # which max/min, converts back to only (1,0) or (-1,0)

    features['MACD6135_gt0'] = np.where(features['MACD6135'] > 0,1,0)
    features['MACD6135_gtSignal'] = np.where(features['MACD6135_hist'] > 0,1,0)
    with np.errstate(invalid='ignore'):
        features['MACD6135_pos0cross'] = np.maximum(features['MACD6135_gt0'].diff(), 0)  # sort of like taking a differential
        features['MACD6135_neg0cross'] = -1 * np.minimum(features['MACD6135_gt0'].diff(), 0)  # because .diff subtracts value - previous row value
        features['MACD6135_possignalcross'] = np.maximum(features['MACD6135_gtSignal'].diff(), 0)  # so takes values of 1 and -1 in current case
        features['MACD6135_negsignalcross'] = -1 * np.minimum(features['MACD6135_gtSignal'].diff(), 0)  # which max/min, converts back to only (1,0) or (-1,0)

    features['MACD362_gt0'] = np.where(features['MACD362'] > 0,1,0)
    features['MACD362_gtSignal'] = np.where(features['MACD362_hist'] > 0,1,0)
    with np.errstate(invalid='ignore'):
        features['MACD362_pos0cross'] = np.maximum(features['MACD362_gt0'].diff(), 0)  # sort of like taking a differential
        features['MACD362_neg0cross'] = -1 * np.minimum(features['MACD362_gt0'].diff(), 0)  # because .diff subtracts value - previous row value
        features['MACD362_possignalcross'] = np.maximum(features['MACD362_gtSignal'].diff(), 0)  # so takes values of 1 and -1 in current case
        features['MACD362_negsignalcross'] = -1 * np.minimum(features['MACD362_gtSignal'].diff(), 0)  # which max/min, converts back to only (1,0) or (-1,0)

    # OLDER LOGIC
    # features['MACDrose_last3days'] = np.where(features['MACD'] > features['MACD'].shift(3),1,0)
    # features['MACDfell_last3days'] = np.where(features['MACD'] < features['MACD'].shift(3),1,0)
    # features['MACDrose_last7days'] = np.where(features['MACD'] > features['MACD'].shift(6),1,0)
    # features['MACDfell_last7days'] = np.where(features['MACD'] < features['MACD'].shift(6),1,0)
    # features['MACDrose_2weeksago'] = np.where(features['MACD'].shift(6) > features['MACD'].shift(13),1,0)
    # features['MACDfell_2weeksago'] = np.where(features['MACD'].shift(6) < features['MACD'].shift(13),1,0)
    # features['MACDposcross_last3days'] = np.where(features['MACDgtSignal'] > features['MACDgtSignal'].shift(3),1,0)
    # features['MACDnegcross_last3days'] = np.where(features['MACDltSignal'] > features['MACDltSignal'].shift(3),1,0)
    # features['MACDposcross_last7days'] = np.where(features['MACDgtSignal'] > features['MACDgtSignal'].shift(6),1,0)
    # features['MACDnegcross_last7days'] = np.where(features['MACDltSignal'] > features['MACDltSignal'].shift(6),1,0)
    # features['MACDposcross_2weeksago'] = np.where(features['MACDgtSignal'].shift(6) > features['MACDgtSignal'].shift(13),1,0)
    # features['MACDnegcross_2weeksago'] = np.where(features['MACDltSignal'].shift(6) > features['MACDltSignal'].shift(13),1,0)

    # conditionsMACDnegative = [
    #     (features['MACD'].shift(7) <= 0), (features['MACD'].shift(8) <= 0), (features['MACD'].shift(9) <= 0),
    #     (features['MACD'].shift(10) <= 0), (features['MACD'].shift(11) <= 0), (features['MACD'].shift(12) <= 0), (features['MACD'].shift(13) <= 0)
    #     ]
    # choicesMACD = [1, 1, 1, 1, 1, 1, 1]
    # features['MACDlt0_2weeksago'] = np.select(conditionsMACDnegative, choicesMACD, default=0)
    #
    # conditionsMACDpositive = [
    #     (features['MACD'].shift(7) >= 0), (features['MACD'].shift(8) >= 0), (features['MACD'].shift(9) >= 0),
    #     (features['MACD'].shift(10) >= 0), (features['MACD'].shift(11) >= 0), (features['MACD'].shift(12) >= 0), (features['MACD'].shift(13) >= 0)
    #     ]
    # choicesMACD = [1, 1, 1, 1, 1, 1, 1]
    # features['MACDgt0_2weeksago'] = np.select(conditionsMACDpositive, choicesMACD, default=0)

    # Features using RSI
    features['RSI_lt30'] = np.where(features['RSI']<=30,1,0)   # "state" feature
    features['RSI_gt70'] = np.where(features['RSI']>=70,1,0)
    with np.errstate(invalid='ignore'):
        features['RSI_crossinto30'] = np.maximum(features['RSI_lt30'].diff(), 0)  # "event" feature
        features['RSI_crossoutof30'] = -1 * np.minimum(features['RSI_lt30'].diff(), 0)
        features['RSI_crossinto70'] = np.maximum(features['RSI_gt70'].diff(), 0)
        features['RSI_crossoutof70'] = -1 * np.minimum(features['RSI_gt70'].diff(), 0)

    conditionsRSI_oversold2weeksago = [
        (features['RSI'].shift(7) <= 30), (features['RSI'].shift(8) <= 30), (features['RSI'].shift(9) <= 30),
        (features['RSI'].shift(10) <= 30), (features['RSI'].shift(11) <= 30), (features['RSI'].shift(12) <= 30), (features['RSI'].shift(13) <= 30)
        ]
    choicesRSI_oversold2weeksago = [1, 1, 1, 1, 1, 1, 1]
    features['RSI_lt30_2weeksago'] = np.select(conditionsRSI_oversold2weeksago, choicesRSI_oversold2weeksago, default=0)

    conditionsRSI_overbought2weeksago = [
        (features['RSI'].shift(7) >= 70), (features['RSI'].shift(8) >= 70), (features['RSI'].shift(9) >= 70),
        (features['RSI'].shift(10) >= 70), (features['RSI'].shift(11) >= 70), (features['RSI'].shift(12) >= 70), (features['RSI'].shift(13) >= 70)
        ]
    choicesRSI_overbought2weeksago = [1, 1, 1, 1, 1, 1, 1]
    features['RSI_gt70_2weeksago'] = np.select(conditionsRSI_overbought2weeksago, choicesRSI_overbought2weeksago, default=0)

    conditions_oversold = [
        (features['RSI'] <= 30), (features['RSI'].shift(1) <= 30), (features['RSI'].shift(2) <= 30), (features['RSI'].shift(3) <= 30),
        (features['RSI'].shift(4) <= 30), (features['RSI'].shift(5) <= 30), (features['RSI'].shift(6) <= 30)
        ]
    choices_oversold = [1, 1, 1, 1, 1, 1, 1]
    features['RSI_lt30_last7days'] = np.select(conditions_oversold, choices_oversold, default=0)

    conditions_overbought = [
        (features['RSI'] >= 70), (features['RSI'].shift(1) >= 70), (features['RSI'].shift(2) >= 70), (features['RSI'].shift(3) >= 70),
        (features['RSI'].shift(4) >= 70), (features['RSI'].shift(5) >= 70), (features['RSI'].shift(6) >= 70)
        ]
    choices_overbought = [1, 1, 1, 1, 1, 1, 1]
    features['RSI_gt70_last7days'] = np.select(conditions_overbought, choices_overbought, default=0)

    # Features using Volume

    features['avgVol_rose_2weeksago'] = np.where(features['avgVol'].shift(6) > features['avgVol'].shift(13),1,0)
    features['avgVol_rose_last7days'] = np.where(features['avgVol'] > features['avgVol'].shift(6),1,0)
    features['avgVol_Volumevsavgvolume'] = features['Total Trade Quantity'] / features.avgVol
    features['avgVol_increase_last7days'] = features.avgVol / features.avgVol.shift(6)

    features['pricevolcorr_gt0'] = np.where(features.pricevolcorr>0,1,0)
    # features['pricevolcorr_lt0'] = np.where(features.pricevolcorr<0,1,0)  # redundant

    # Volume-related events
    features['pricevolcorr_poscross'] = np.maximum(features['pricevolcorr_gt0'].diff(), 0)
    features['pricevolcorr_negcross'] = -1 * np.minimum(features['pricevolcorr_gt0'].diff(), 0)

    # continuous var
    features['pricevolcorr_delta'] = features['pricevolcorr'].diff()  # sort of a differential of pricevolcorr

    features['pricevolcorr_siginc'] = np.where(features['pricevolcorr_delta'] > 0.5, 1,0)  # significant increase in pricevolcorr
    features['pricevolcorr_sigdec'] = np.where(features['pricevolcorr_delta'] < -0.5, 1,0)  # significant decrease in pricevolcorr

    # not working
    # features['RSI<30inlast7days'] = np.where(any(x <= 30 for x in features['RSI'][-7:].values), 1, 0)

    # Basically, need to figure out a good way of describing the temporal relaionships between the various indicators or other features
    # Esp. using lookback_period
    # Features describing overall market
    # 'NSE/CNX_NIFTY' trend
    # features['nifty_volatility'] = nifty_data['daily_volatility']
    # features['nifty_MACDgtSignal'] = 0
    # features['nifty_MACDgtSignal'] = features['nifty_MACDgtSignal'].where(nifty_data['MACD'] < nifty_data['MACDsignal'],other=1)
    # features['nifty_MACDltSignal'] = 0
    # features['nifty_MACDltSignal'] = features['nifty_MACDltSignal'].where(nifty_data['MACD'] > nifty_data['MACDsignal'],other=1)
    # features['nifty_MACDgt0'] = 0
    # features['nifty_MACDgt0'] = features['nifty_MACDgt0'].where(nifty_data['MACD'] < 0,other = 1)
    # features['nifty_MACDlt0'] = 0
    # features['nifty_MACDlt0'] = features['nifty_MACDlt0'].where(nifty_data['MACD'] > 0,other = 1)
    # Above statements use pandas.Series.where(), which keeps the value if True, and replaces with "other" when false

    features['marketbreadthfast'] = market_breadth_fast  # confirmed that pandas automatically takes the value for the right index/date
    features['marketbreadthfast_gt0'] = np.where(features['marketbreadthfast'] > 0,1,0)

    features['marketbreadthslow'] = market_breadth_slow  # confirmed that pandas automatically takes the value for the right index/date
    features['marketbreadthslow_gt0'] = np.where(features['marketbreadthslow'] > 0,1,0)

    # Features describing volatility

    # features['volatility_by_niftyvolatility'] = features['daily_volatility'] / features['nifty_volatility']

    features['volatilityrose_2weeksago'] = np.where(features['daily_volatility'].shift(6) > features['daily_volatility'].shift(13),1,0)
    features['volatilityrose_last7days'] = np.where(features['daily_volatility'] > features['daily_volatility'].shift(6),1,0)
    features['volatilityincrease_last7days'] = features['daily_volatility'] / features['daily_volatility'].shift(6)

    # Most recent daily return
    features['onedayreturn_gt0'] = np.where(features['log_daily_returns'] > 0,1,0)

    # use np.where for one condition or np.select to combine several conditions

    # Clipping initial bit which doesn't have accurate features
    features = features.loc[features.index.min() + datetime.timedelta(lookback_period):]

    return features

