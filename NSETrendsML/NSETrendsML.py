import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')

"""
A note about project structure

All source code files are in NSETrendsML/NSETrendsML folder
This means that in order to import a module from this folder PYTHONPATH must include NSETrendsML/NSETrendsML
In PyCharm, this is achieved by making this folder "Sources Root", and 
checking the option for "source root" in the settings, so that sys.path.extend is executed at the beginning

# The os.chdir line is not required in PythonAnywhere (live) version, but required while running from Pycharm console
# They are required to avoid the file not found error reading or writing to project folders, e.g 
# 1) reading the client secret in prepare.py
# 2) reading or writing JSON strategies
# 3) writing tree pngs or graph to file
"""

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')  # must be called before importing pyplot
from matplotlib import pyplot as plt
from functions import applyEntry
from functions import scoreExit
from functions import printPerformance

from functions import combine
from functions import getFeaturesfromConditions
from functions import getOverallPerformance
from functions import saveStrategy
from functions import printStrategy
from functions import getConditionsfromTree

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.tree import export_graphviz  # To really use this, Graphviz must be installed separately on Local
import pydotplus
from sklearn.externals.six import StringIO

from candlestick import plot_random_signal
import datetime
import json  # to save strategies to file

from functions import get_monthly_performance
from functions import get_monthly_performance_from_pnl
from functions import get_daily_pnl

# from pandarallel import pandarallel

# To get a fresh pickle, run "prepare.py"

# Or read from existing pickle
signalsdf = pd.read_pickle('latest_scored_10years.pickle')
signalsdf = pd.read_pickle('NSEdata_50_APR7_withtechnicals.pickle')
signalsdf = pd.read_pickle('NSEdata_50_APR7_withfeatures.pickle')
signalsdf = pd.read_pickle('NSEdata_50_APR7_withMAX1')
signalsdf = pd.read_pickle('NSEdata_50_APR7_withMAX10')

startdate = datetime.datetime.strptime("2009-05-10", "%Y-%m-%d")
signalsdf = signalsdf.loc[signalsdf.index.get_level_values(level = 'Date') >= startdate]

signalsdf = pd.read_pickle('latest_scored.pickle')
signalsdf = signalsdf.loc[signalsdf.index.get_level_values(level = 'Date') >= datetime.datetime.strptime("2014-01-01", "%Y-%m-%d")]
signalsdf = signalsdf.loc[signalsdf.index.get_level_values(level = 'Date') <= datetime.datetime.strptime("2019-01-01", "%Y-%m-%d")]
# signalsdf = signalsdf.loc[signalsdf.index.get_level_values(level = 'Ticker') != 'YESBANK']

performance_vars = ['buy_price','sell_price','signal_return','held_for_days','winloss','winloss2']

for var in performance_vars:
    signalsdf[var] = 0  # initialize performance vars

# to get back to NSEdata et. al (dict of dfs) from unpickled signalsdf
NSEdatawithfeatures = {}
for ticker in signalsdf.index.levels[0]:
    if ticker != 'benchmark':
        NSEdatawithfeatures[ticker] = signalsdf.loc[ticker]

# print keys
print(signalsdf.keys())
# for this pickle (it has features and benchmark, once features are pulled, benchmark can be dropped)
# signalsdf = signalsdf.drop(labels='benchmark')


# ------------ above code runs before any backtesting


entryConditions = [['CredM','<',0]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
# trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-1],['cross_val','==',-2.5]],'|'),'MIN0','None','None'),axis=1)
# of signals =      86  Winrate =  58.1%  Avg return =  1.19%  Avg days of holding =  8.9  Avg win return =  3.64%  Avg lose return =  -2.22%  Std dev return =  0.05  Skew return =  1.36  Kurtosis return =  6.73
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['CredM','>',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)


entryConditions = [['cross_val','==',-2],['cross_sign','==',1],['marketbreadthfast','>',0.2]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
# trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-1],['cross_val','==',-2.5]],'|'),'MIN0','None','None'),axis=1)
# of signals =      86  Winrate =  58.1%  Avg return =  1.19%  Avg days of holding =  8.9  Avg win return =  3.64%  Avg lose return =  -2.22%  Std dev return =  0.05  Skew return =  1.36  Kurtosis return =  6.73
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)


entryConditions = [['cross_val','==',-1.5],['cross_sign','==',-1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',0],['cross_val','==',-2]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)

entryConditions = [['cross_val','==',-1.5],['cross_sign','==',1],['marketbreadthfast','>',0.1]]  # positive crossover of -1.5 in a bull market
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',0],['cross_val','==',-2]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)


# Something that works:
entryConditions = [['cross_val','==',-1],['cross_sign','==',1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-2],['cross_val','==',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     904  Winrate =  62.5%  Avg return =  0.79%  Avg days of holding =  10.3  Avg win return =  3.26%  Avg lose return =  -3.39%  Std dev return =  0.05  Skew return =  -0.73  Kurtosis return =  7.84
"""
# of signals =     898  Winrate =  62.9%  Avg return =  0.79%  Avg days of holding =  10.3  Avg win return =  3.26%  Avg lose return =  -3.39%  Std dev return =  0.05  Skew return =  -0.73  Kurtosis return =  7.84
Annualized Return (%)
16.8 %
Annualized Volatility
12.6
Annualized Sharpe Ratio assuming 7% risk-free return
0.624
Annualized Sharpe Ratio assuming 0% risk-free return
1.104
Value-at-Risk Monthly 95% confidence (%)
-4.8 %
"""

# Extracting the bull part
entryConditions = [['cross_val','==',-1],['cross_sign','==',1],['marketbreadthfast','>',0.1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-2],['cross_val','==',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     396  Winrate =  62.1%  Avg return =  0.81%  Avg days of holding =  10.1  Avg win return =  3.42%  Avg lose return =  -3.55%  Std dev return =  0.05  Skew return =  -1.46  Kurtosis return =  9.90
"""
# of signals =     393  Winrate =  62.6%  Avg return =  0.81%  Avg days of holding =  10.1  Avg win return =  3.42%  Avg lose return =  -3.55%  Std dev return =  0.05  Skew return =  -1.46  Kurtosis return =  9.90
Annualized Return (%)
8.808614172264646
Annualized Volatility
8.153756974965722
Annualized Sharpe Ratio assuming 7% risk-free return
0.15381347261511447
Annualized Sharpe Ratio assuming 0% risk-free return
0.8980981086296326
Value-at-Risk Monthly 95% confidence (%)
-3.4164364702733825
"""
trades_bull = trades

# Extracting the bear part
entryConditions = [['cross_val','==',-1],['cross_sign','==',1],['marketbreadthfast','<',-0.1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-2],['cross_val','==',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     412  Winrate =  64.6%  Avg return =  0.97%  Avg days of holding =  10.5  Avg win return =  3.09%  Avg lose return =  -2.90%  Std dev return =  0.04  Skew return =  0.24  Kurtosis return =  6.73
"""
# of signals =     412  Winrate =  64.6%  Avg return =  0.97%  Avg days of holding =  10.5  Avg win return =  3.09%  Avg lose return =  -2.90%  Std dev return =  0.04  Skew return =  0.24  Kurtosis return =  6.73
Annualized Return (%)
8.808614172264646
Annualized Volatility
8.153756974965722
Annualized Sharpe Ratio assuming 7% risk-free return
0.24341655589735361
Annualized Sharpe Ratio assuming 0% risk-free return
1.0007326654769715
Value-at-Risk Monthly 95% confidence (%)
-3.17
"""
trades_bear = trades

# Both are trend following systems looks like, below we combine them but with different cut offs (for bullish market, be in the trade longer
entryConditions = [['cross_val','==',-1],['cross_sign','==',1],['marketbreadthfast','>',0.1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-2.5],['cross_val','==',0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     396  Winrate =  64.9%  Avg return =  1.14%  Avg days of holding =  21.8  Avg win return =  4.94%  Avg lose return =  -6.25%  Std dev return =  0.08  Skew return =  -0.61  Kurtosis return =  3.36

trades_bull2 = trades

# Both are trend following systems looks like, below we combine them but with different cut offs (for bearish market, be in the trade for less time)
entryConditions = [['cross_val','==',-1],['cross_sign','==',1],['marketbreadthfast','<',0.1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['cross_val','==',-1.5],['cross_val','==',-0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     533  Winrate =  57.6%  Avg return =  0.40%  Avg days of holding =  5.8  Avg win return =  2.40%  Avg lose return =  -2.34%  Std dev return =  0.04  Skew return =  0.34  Kurtosis return =  13.71

trades_bear2 = trades

# combine
trades = pd.concat([trades_bull2,trades_bear2])

#### Beginning - Strategy Performance Estimations ####

# getting a daily P&L from a backtest result ("trades")

total_capital = 100000
capital_per_trade = 5000

trades = trades[~np.isnan(trades.signal_return.values)]  # removing values with no performance
PnL = get_daily_pnl(trades,NSEdatawithfeatures,total_capital,capital_per_trade)
fig, ax = plt.subplots(nrows=5,ncols=1)
# plt.plot(PnL[['nifty','Cash','MTM']])
# ax[0].plot(PnL[['portfolio_value','nifty']]); ax[0].set_ylabel('portfolio vs nifty')
ax[0].plot(PnL[['portfolio_value']]); ax[0].set_ylabel('portfolio'); ax[0].axhline(y=total_capital, color='red', linewidth=0.5)
ax[1].plot(PnL[['realizedProfit']]); ax[1].set_ylabel('realizedProfit')
ax[2].plot(PnL[['unrealizedProfit']]); ax[2].set_ylabel('unrealizedProfit'); ax[2].axhline(y=0, color='red', linewidth=0.5)
# ax[3].plot(PnL[['profit']]); ax[3].set_ylabel('Total Profit')
ax[3].plot(PnL[['exposedPrincipal']]); ax[3].set_ylabel('exposedPrincipal'); ax[3].axhline(y=total_capital, color='red', linewidth=0.5)

PnL['daily_return'] = np.nan_to_num((PnL['profit'] - PnL['profit'].shift(1))/PnL['exposure'].shift(1))
PnL['nifty_daily_return'] = np.nan_to_num((PnL['nifty'] - PnL['nifty'].shift(1))/PnL['nifty'].shift(1))
# Above is not accurate but keeping because function get_monthly_performance_from_pnl expects it
PnL['daily_return'] = np.nan_to_num((PnL['portfolio_value'] - PnL['portfolio_value'].shift(1))/PnL['portfolio_value'].shift(1))

monthlypnl = get_monthly_performance_from_pnl(PnL) # Not sure if correct - read notes in function
# ax.plot(monthlypnl[['Monthly Return','Monthly Nifty Return']])
ax[4].plot(monthlypnl[['Monthly Return']]); ax[4].set_ylabel('monthly return')
ax[4].axhline(y=0, color='black', linewidth=0.5)

annualized_return = PnL.daily_return.mean() * 365 * 100  # Calculate annualized daily return
annualized_volatility = PnL.daily_return.std() * np.sqrt(365) * 100  # Calculate annualized volatility

# Calculate VaR
from scipy.stats import norm
mean_daily_return = np.mean(PnL['daily_return'])
std_daily_return = np.std(PnL['daily_return'])
VaR95_daily_varcovar = norm.ppf(1-0.95,np.mean(PnL['daily_return']),np.std(PnL['daily_return']))
Var95_daily_historical = PnL['daily_return'].sort_values().quantile((0.05))

# Calculate monthly or daily-return Sharpe ratio using risk free return of 7%

# np.mean(PnL['nifty_daily_return']) / np.std(PnL['nifty_daily_return'])   # daily return Sharpe Ratio of Nifty
daily_sharpe = np.mean(PnL['daily_return']) / np.std(PnL['daily_return'])   # daily return Sharpe Ratio of strategy
# for annual
annualized_daily_sharpe = np.sqrt(252) * np.mean(PnL['daily_return']) / np.std(PnL['daily_return'])
# Assuming risk free interest rate of 7%
annualized_daily_sharpe_rf = np.sqrt(252) * (np.mean(PnL['daily_return']) - 0.0002) / np.std(PnL['daily_return'])  # 0.002 = 7%/365

print("")
printPerformance(trades)
print("Annualized Return (%)")
print(annualized_return)
print("Annualized Volatility")
print(annualized_volatility)
print("Annualized Sharpe Ratio assuming 7% risk-free return")
print(annualized_daily_sharpe_rf)
print("Annualized Sharpe Ratio assuming 0% risk-free return")
print(annualized_daily_sharpe)
print("Value-at-Risk Monthly 95% confidence (%)")
print(Var95_daily_historical * np.sqrt(30) * 100)


#### End - Strategy Performance ####


















# Run these for market context based strategies:

# Bull trend - buy when its too low compared to market, hold till stock catches up with market
entryConditions = [['conviction_recent','>',1],['conviction_recent','<',2],['marketbreadthfast','>',0],['marketbreadthfast','<',0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','<',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     187  Winrate =  64.2%  Avg return =  1.67%  Avg days of holding =  17.6  Avg win return =  4.86%  Avg lose return =  -4.73%  Std dev return =  0.06  Skew return =  -1.22  Kurtosis return =  4.68

# Bear trend
entryConditions = [['conviction_recent','<',-0.5],['conviction_recent','>',-1.5],['marketbreadthfast','<',0],['marketbreadthfast','>',-0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     418  Winrate =  64.1%  Avg return =  0.90%  Avg days of holding =  18.5  Avg win return =  4.22%  Avg lose return =  -5.50%  Std dev return =  0.06  Skew return =  -0.89  Kurtosis return =  3.09

# Overbought
entryConditions = [['conviction_recent','<',-1.5]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',-0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =      77  Winrate =  68.8%  Avg return =  1.17%  Avg days of holding =  11.7  Avg win return =  3.77%  Avg lose return =  -5.08%  Std dev return =  0.05  Skew return =  -1.07  Kurtosis return =  1.73

# Oversold
entryConditions = [['conviction_recent','>',2]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','<',0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =      58  Winrate =  72.4%  Avg return =  1.98%  Avg days of holding =  11.6  Avg win return =  3.75%  Avg lose return =  -2.65%  Std dev return =  0.04  Skew return =  -0.02  Kurtosis return =  0.88

# Remaining - slightly less than 0 (market average)
entryConditions = [['conviction_recent','>',-0.5],['conviction_recent','<',0]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     808  Winrate =  56.4%  Avg return =  0.12%  Avg days of holding =  13.1  Avg win return =  2.96%  Avg lose return =  -3.88%  Std dev return =  0.05  Skew return =  -1.11  Kurtosis return =  5.08

# Remaining - slightly greater than 0 (market average)
entryConditions = [['conviction_recent','<',1],['conviction_recent','>',0]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','<',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     605  Winrate =  62.5%  Avg return =  0.54%  Avg days of holding =  14.0  Avg win return =  3.22%  Avg lose return =  -4.27%  Std dev return =  0.05  Skew return =  -1.40  Kurtosis return =  5.68

# Bear trend variant
entryConditions = [['conviction_recent','<',-1],['conviction_recent','>',-1.5],['marketbreadthfast','<',0],['marketbreadthfast','>',-0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',-0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     209  Winrate =  63.2%  Avg return =  0.91%  Avg days of holding =  12.4  Avg win return =  3.73%  Avg lose return =  -4.04%  Std dev return =  0.06  Skew return =  0.34  Kurtosis return =  5.52


# oversold in a bull market - using crossovers
entryConditions = [['CredM','<',-2],['Vol','>',20],['marketbreadthfast','>',0],['marketbreadthfast','<',0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),0,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['CredM','>',-1.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)

# Bull trend
entryConditions = [['CredM','>',0.9], ['CredM','<',1.1]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),0,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['CredM','<',0],['CredM','>',2]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     187  Winrate =  64.2%  Avg return =  1.67%  Avg days of holding =  17.6  Avg win return =  4.86%  Avg lose return =  -4.73%  Std dev return =  0.06  Skew return =  -1.22  Kurtosis return =  4.68



# overbought in a bear market
entryConditions = [['CredM','>',1.5],['Vol','>',10],['marketbreadthfast','<',0],['marketbreadthfast','>',-0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['CredM','<',0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     418  Winrate =  64.1%  Avg return =  0.90%  Avg days of holding =  18.5  Avg win return =  4.22%  Avg lose return =  -5.50%  Std dev return =  0.06  Skew return =  -0.89  Kurtosis return =  3.09

# Overbought
entryConditions = [['conviction_recent','<',-1.5]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',-0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =      77  Winrate =  68.8%  Avg return =  1.17%  Avg days of holding =  11.7  Avg win return =  3.77%  Avg lose return =  -5.08%  Std dev return =  0.05  Skew return =  -1.07  Kurtosis return =  1.73

# Oversold
entryConditions = [['conviction_recent','>',2]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','<',0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =      58  Winrate =  72.4%  Avg return =  1.98%  Avg days of holding =  11.6  Avg win return =  3.75%  Avg lose return =  -2.65%  Std dev return =  0.04  Skew return =  -0.02  Kurtosis return =  0.88

# Remaining - slightly less than 0 (market average)
entryConditions = [['conviction_recent','>',-0.5],['conviction_recent','<',0]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     808  Winrate =  56.4%  Avg return =  0.12%  Avg days of holding =  13.1  Avg win return =  2.96%  Avg lose return =  -3.88%  Std dev return =  0.05  Skew return =  -1.11  Kurtosis return =  5.08

# Remaining - slightly greater than 0 (market average)
entryConditions = [['conviction_recent','<',1],['conviction_recent','>',0]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','<',0]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     605  Winrate =  62.5%  Avg return =  0.54%  Avg days of holding =  14.0  Avg win return =  3.22%  Avg lose return =  -4.27%  Std dev return =  0.05  Skew return =  -1.40  Kurtosis return =  5.68

# Bear trend variant
entryConditions = [['conviction_recent','<',-1],['conviction_recent','>',-1.5],['marketbreadthfast','<',0],['marketbreadthfast','>',-0.7]]
trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'short')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['conviction_recent','>',-0.5]],'|'),'MIN0','None','None'),axis=1)
printPerformance(trades)
# of signals =     209  Winrate =  63.2%  Avg return =  0.91%  Avg days of holding =  12.4  Avg win return =  3.73%  Avg lose return =  -4.04%  Std dev return =  0.06  Skew return =  0.34  Kurtosis return =  5.52



#  Till here


trades.reset_index()[['Ticker','Date','proba_rf','predicted_return','CredM']+performance_vars].sort_values(by='Date').to_csv('perf.csv')

# For all signals
trades = signalsdf.copy()
trades['longshort'] = 'long'
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX10','None','None'),axis=1)
trades['winloss5'] = np.where(trades['signal_return'] > 0.05,1,0)

# After this, modeling.py

# For getting signals of a specific period (e.g. for out of sample validation etc.)
start = datetime.datetime(2019, 1, 1, 0, 0)
stop = datetime.datetime(2019, 12, 31, 0, 0)
signalsdf_sub = signalsdf.iloc[signalsdf.index.get_level_values('Date') >= start]
signalsdf_sub = signalsdf_sub.iloc[signalsdf_sub.index.get_level_values('Date') <= stop]

trades = signalsdf_sub
trades['longshort'] = 'long'
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX10','static1','static2'),axis=1)













# Below was written for 'processizing' the modeling - creating a lot of decision trees by hand

# setup repeat experiments

# listing features by type
discrete_events = ['MACD12269_pos0cross', 'MACD12269_neg0cross', 'MACD12269_possignalcross', 'MACD12269_negsignalcross', 'MACD245218_pos0cross', 'MACD245218_neg0cross', 'MACD245218_possignalcross', 'MACD245218_negsignalcross', 'MACD6135_pos0cross', 'MACD6135_neg0cross', 'MACD6135_possignalcross', 'MACD6135_negsignalcross', 'MACD362_pos0cross', 'MACD362_neg0cross', 'MACD362_possignalcross', 'MACD362_negsignalcross', 'RSI_crossinto30', 'RSI_crossoutof30', 'RSI_crossinto70', 'RSI_crossoutof70', 'pricevolcorr_poscross', 'pricevolcorr_negcross', 'pricevolcorr_siginc', 'pricevolcorr_sigdec']
discrete_states = ['MACD12269_gt0', 'MACD12269_gtSignal', 'MACD245218_gt0', 'MACD245218_gtSignal', 'MACD6135_gt0', 'MACD6135_gtSignal', 'MACD362_gt0', 'MACD362_gtSignal', 'RSI_lt30', 'RSI_gt70', 'RSI_lt30_2weeksago', 'RSI_gt70_2weeksago', 'RSI_lt30_last7days', 'RSI_gt70_last7days', 'avgVol_rose_2weeksago', 'avgVol_rose_last7days', 'pricevolcorr_gt0', 'marketbreadthfast_gt0', 'marketbreadthslow_gt0', 'volatilityrose_2weeksago', 'volatilityrose_last7days', 'onedayreturn_gt0']
continuous = ['daily_volatility', 'pricevolcorr', 'avgVol_Volumevsavgvolume', 'avgVol_increase_last7days', 'pricevolcorr_delta', 'marketbreadthfast', 'marketbreadthslow', 'volatilityincrease_last7days']

# For one entry condition based on features
entryConditions = [['MACD245218_pos0cross','==',1]]

# For entry condition based on probs
entryConditions = [['proba_rf','>',0.58],['predicted_return','>',0.01]]

trades = applyEntry(signalsdf,combine(entryConditions,'&'),10,'long')
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([['proba_rf','<',0.54],['predicted_return','<',0]],'|'),'MAX10','None','None'),axis=1)
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX10','None','None'),axis=1)


'''

1. Each model should have MACD, RSI, volume and volatility
For 6 separate entry conditions, each for long and short, each with MAX10 or reverse condition + MAX20, no stoploss, target
1a. Model start should have one initial_entry_condition, and original days 0
1b And one set of initial_exit_condition
1c And should output one set of strategy_entry_condition

3. This should be followed by regression against the initial entry conditions’s underlying continuous vars to give a model of confidence level

2 4X Finally, final_exit_conditions, including static or trailing stoploss based on volatility, static trailing 22 and 24

4. The distribution of returns (and holding period) whether it shows high winrate with low positive skew, or small winrate with high positive skew
Skew and kurtosis need to be measured (instead of avg returns), and whether confidence level is correlated with

'''

def runDTwithfeatures(to_model,treefilename, features_to_model):

    to_model = to_model.reset_index()
    # test for missing features
    for key in features_to_model:
        a = to_model[key].isnull().sum()
        b = len(to_model[key])
        if a > 0:
            print(key + " is missing " + str(a) + " out of " + str(len(to_model[key])) + " values")
    # if too many, drop the column itself first
    to_model = to_model.drop(columns=[])
    # if not too many, drop the rows
    to_model.dropna(inplace=True)  # drop rows with missing values
    # Prepare datasets for modeling
    X = to_model[features_to_model]
    np.isnan(X).any()  # is all false
    y = to_model['winloss']
    # y = to_model['signal_return']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)
    # for i in range(1,10,1):
    clf_gini = DecisionTreeClassifier(criterion="gini", random_state=10, max_depth=5, min_samples_leaf=50)
    clf_gini.fit(X_train, y_train)
    y_pred = clf_gini.predict(X_test)
    print(accuracy_score(y_test, y_pred) * 100)
    # visualize
    dot_data = StringIO()
    export_graphviz(clf_gini, out_file=dot_data, filled=True, feature_names=features_to_model)
    graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
    graph.write_png(treefilename)  # had to install Graphviz on laptop
    # convert to conditions
    conditions_list = getConditionsfromTree(clf_gini, features_to_model)

    return conditions_list

# list_of_initial_strategies = [{'entry': [['RSIlt30','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['RSIgt70','==',1]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['MACDposcross_last3days','==',1],['market_breadth_positive','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['MACDnegcross_last3days','==',1],['market_breadth_positive','==',0]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['onedayreturn_gt0','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['onedayreturn_gt0','==',1]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['onedayreturn_gt0','==',1]], 'longshort': 'long', 'exit': [['onedayreturn_gt0','==',0]], 'timelimit': 'MAX10'},
#                               {'entry': [['onedayreturn_gt0','==',1]], 'longshort': 'short', 'exit': [['onedayreturn_gt0','==',0]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumevsavgvolume','>',2.5]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumevsavgvolume','>',2.5]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumevsavgvolume','>',2.5]], 'longshort': 'long', 'exit': [['Volumevsavgvolume','<',0.7]], 'timelimit': 'MIN0'},
#                               {'entry': [['Volumevsavgvolume','>',2.5]], 'longshort': 'short', 'exit': [['Volumevsavgvolume','<',0.7]], 'timelimit': 'MIN0'},
#                               ]

# list_of_initial_strategies = [{'entry': [['MACDposcross_last3days','==',1],['market_breadth_positive','==',0]], 'longshort': 'long', 'exit': [['MACDfell_last7days','==',0]], 'timelimit': 'MAX20'},
#                               {'entry': [['MACDgt0','==',1], ['MACDfell_last7days','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['MACDgt0','==',1], ['MACDfell_last7days','==',1]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['MACDlt0','==',1], ['MACDfell_last7days','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['MACDlt0','==',1], ['MACDfell_last7days','==',1]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumeincrease_last7days','>',1.5]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumeincrease_last7days','>',1.5]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumeincrease_last7days','<',0.7]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['Volumeincrease_last7days','<',0.7]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIlt30','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIlt30','==',1]], 'longshort': 'long', 'exit': [['pricevolcorrgt0','==',0]], 'timelimit': 'MAX10'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIgt70','==',1]], 'longshort': 'short', 'exit': [[]], 'timelimit': 'MAX10'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIgt70','==',1]], 'longshort': 'short', 'exit': [['pricevolcorrgt0','==',0]], 'timelimit': 'MAX10'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIgt70','==',1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX5'},
#                               {'entry': [['pricevolcorrgt0','==',1], ['RSIgt70','==',1]], 'longshort': 'long', 'exit': [['pricevolcorrgt0','==',0]], 'timelimit': 'MAX5'},
#                               ]

list_of_initial_strategies = [
                              {'entry': [['MACD12269_possignalcross', '==', 1], ['marketbreadthfast_gt0', '==', 1]], 'longshort': 'long', 'exit': [[]], 'timelimit': 'MAX5'},
                              ]

count = 0

trend_features = ['MACDgtSignal', 'MACDltSignal', 'MACDgt0', 'MACDlt0', 'MACDrose_last3days', 'MACDfell_last3days',
       'MACDrose_last7days', 'MACDfell_last7days', 'MACDrose_2weeksago', 'MACDfell_2weeksago', 'MACDposcross_last3days',
       'MACDnegcross_last3days', 'MACDposcross_last7days', 'MACDnegcross_last7days', 'MACDposcross_2weeksago',
       'MACDnegcross_2weeksago', 'MACDlt0_2weeksago', 'MACDgt0_2weeksago']
momentum_features = ['RSIlt30', 'RSIlt30_2weeksago', 'RSIgt70', 'RSIgt70_2weeksago', 'RSIlt30_last7days', 'RSIgt70_last7days']
volume_features = ['Volumerose_2weeksago', 'Volumerose_last7days', 'Volumevsavgvolume', 'Volumeincrease_last7days', 'pricevolcorrgt0', 'pricevolcorrlt0']
volatility_features = ['volatilityrose_2weeksago', 'volatilityrose_last7days', 'volatilityincrease_last7days']
# other_features = ['onedayreturn_gt0', 'market_breadth_positive']


trend_features = ['MACD12269_gt0', 'MACD12269_gtSignal', 'MACD12269_pos0cross', 'MACD12269_neg0cross',
       'MACD12269_possignalcross', 'MACD12269_negsignalcross',
       'MACD245218_gt0', 'MACD245218_gtSignal', 'MACD245218_pos0cross',
       'MACD245218_neg0cross', 'MACD245218_possignalcross',
       'MACD245218_negsignalcross', 'MACD6135_gt0', 'MACD6135_gtSignal',
       'MACD6135_pos0cross', 'MACD6135_neg0cross', 'MACD6135_possignalcross',
       'MACD6135_negsignalcross', 'MACD362_gt0', 'MACD362_gtSignal',
       'MACD362_pos0cross', 'MACD362_neg0cross', 'MACD362_possignalcross', 'MACD362_negsignalcross']
momentum_features = ['RSI_lt30', 'RSI_gt70', 'RSI_crossinto30',
       'RSI_crossoutof30', 'RSI_crossinto70', 'RSI_crossoutof70',
       'RSI_lt30_2weeksago', 'RSI_gt70_2weeksago', 'RSI_lt30_last7days',
       'RSI_gt70_last7days']
volume_features = ['avgVol_rose_2weeksago', 'avgVol_rose_last7days',
       'avgVol_Volumevsavgvolume', 'avgVol_increase_last7days',
       'pricevolcorr_gt0', 'pricevolcorr_poscross', 'pricevolcorr_negcross',
       'pricevolcorr_delta', 'pricevolcorr_siginc', 'pricevolcorr_sigdec']
volatility_features = ['volatilityrose_2weeksago', 'volatilityrose_last7days', 'volatilityincrease_last7days']
# other_features = ['onedayreturn_gt0', 'marketbreadthfast', 'marketbreadthfast_gt0', 'marketbreadthslow', 'marketbreadthslow_gt0']

feature_list = [trend_features,momentum_features,volume_features,volatility_features]

# pandarallel.initialize()

for strategy in list_of_initial_strategies:
    entry = strategy['entry']
    longshort = strategy['longshort']
    longshort = 'long'
    exit = strategy['exit']
    timelimit = strategy['timelimit']
    print(entry, longshort, exit, timelimit)

    trades = applyEntry(signalsdf,combine(entry,'&'), 0, longshort)
    trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures, combine(exit, '|'), timelimit, 'None', 'None'), axis=1)
    # trades[performance_vars] = trades.parallel_apply(scoreExit,args=(NSEdatawithfeatures, combine(exit, '|'), timelimit, 'None', 'None'), axis=1)

    strategy_entry_conditions = entry
    for features in feature_list:
        features = [item for sublist in feature_list for item in sublist]
        features_to_model = [x for x in features if (x not in entry)]
        print('running decision tree')
        conditions_list = runDTwithfeatures(trades, 'tree.png', features_to_model)
        conditions_with_max_winrate = max(conditions_list, key=lambda x: x['winrate'])['conditions']
        strategy_entry_conditions += conditions_with_max_winrate
    print(strategy_entry_conditions)
    trades = applyEntry(signalsdf, combine(strategy_entry_conditions, '&'), 5, longshort)

    for stoploss, target in [['static2','static2'], ['static2', 'static4'], ['trailing2','trailing2'], ['trailing2','trailing4']]:
        count += 1
        print(stoploss, target)
        trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures, combine(exit, '|'), timelimit, stoploss, target), axis=1)
        performance = getOverallPerformance(trades)
        strategy_name = "strategy"+str(count)
        print(strategy_name)
        strategy = saveStrategy(strategy_name,longshort,strategy_entry_conditions,exit,timelimit,stoploss,target,performance)
        filename = os.path.join('outputs', strategy['name'] + '.json')
        with open(filename, 'w') as fp:
            json.dump(strategy, fp)



# Reading all strategies

num_list = [2,	3,	4,	5,	7,	8,	9,	10,	12,	13,	14,	15,	17,	18,	19,	20,	22,	23,	24,	25,	27,	28,	29,	30,	33,	34,	35,	36,	38,	39,	40,	41,	43,	44,	45,	46,	48,	49,	50,	51]
num_list = num_list + list(range(53,117))
strategy_list = []
for num in num_list:
    # print(num)
    filename = os.path.join('outputs2', 'strategy' + str(num) + '.json')
    with open(filename) as json_data:
        strategy_list.append(json.load(json_data))

# print entry conditions of all strategies
for strategy in strategy_list:
    if (strategy['stoploss'] == 'static2') and (strategy['target'] == 'static2') and (strategy['longshort'] == 'short'):
        print(strategy['entry'])



performance_dict_list = []
for strategy in strategy_list:
    perf = strategy['performance']
    perf['name'] = strategy['name']
    perf['longshort'] = strategy['longshort']
    perf['ST'] = strategy['stoploss']+strategy['target']
    performance_dict_list.append(perf)
perf_allstrats = pd.DataFrame(performance_dict_list)

perf_allstrats.to_csv('perf_allstrats.csv')  # moved to excel for studying relation between ST, longshort and performance

# understanding a strategy (strategy25)

# Good winning strategies are strategy57 and strategy89, both long

strategy = strategy_list[19]
strategy = strategy_list[85]
printStrategy(strategy)
trades = applyEntry(signalsdf,combine(strategy['entry'],'&'),5,strategy['longshort'])
trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(strategy['exit'],'|'),
                                                        strategy['timelimit'],strategy['stoploss'],strategy['target']),axis=1)

trades[performance_vars] = trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(strategy['exit'],'|'),'MIN0','trailing2','trailing4'),axis=1)

printPerformance(trades)
# number of signals by month
trades1 = trades.reset_index(level=['Ticker']).groupby(pd.Grouper(freq='M')).aggregate({'Ticker': {'Signals': 'count'}})
plt.plot(trades1)
# (1) no months with no signal, but (2) used to be a lot of signals but have reduced in recent years

# distribution of returns
plt.hist(trades.reset_index().signal_return,bins=10)
plt.hist(trades.reset_index().held_for_days,bins=10)

# observe outliers
a = trades[trades.signal_return < 0.15]
plot_random_signal(a,['pricevolcorr','Total Trade Quantity'],signalsdf)

# do they bunch up in time or are spread out (plus ideas about how to combine two strategies to reduce the bunching up
# what is the distribution of returns (plus ideas about how to change it using exit) - are their fewer but bigger wins, vs more but smaller ones
# what is the distribution of holding days
# what is the "relative" impact of stoploss/targets on this distribution
# which is the "best" strategy

# are two strategies actually the same (see overlapping signals)
# is there an impact of getting a deeper decision tree on overall data rather than four different types
# can a confidence number be calculated by regressing the vars against signal_return or winloss
# can winloss be changed to winloss3 (win only if 3% or more return)
# what if MAX20 instead of MAX10 was used as initial exit
# does monthly return make sense - VaR, Sharpe, etc.
# does a strategy stand an "out of sample" test!
# does removing duplicate signals before decision tree change a model?

features_to_plot = []
for conditions in strategy['entry']:
    features_to_plot.append(conditions[0])
plot_random_signal(trades,features_to_plot,signalsdf)

entryConditions = [['market_breadth_positive', '==', 1],['RSIlt30', '==', 1]]
entryConditions = [['RSI', '<', 20]]
entryConditions = [['onedayreturn_gt0', '==', 0],['RSIlt30', '==', 1],['Volumerose_last7days', '==', 0],
                   ['MACDnegcross_last7days', '==', 1],['volatilityrose_2weeksago', '==', 1],['RSIlt30_2weeksago', '<', 1]]


entryConditions = [['MACD12269_possignalcross','==',1]]
long_trades = applyEntry(signalsdf,combine(entryConditions,'&'),0,'long')
long_trades[performance_vars] = long_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX10','None','None'),axis=1)

subset_original_days = 5  # original_days

long_trades = applyEntry(signalsdf,combine(entryConditions,'&'),subset_original_days,'long')

exit_conditions = [[]]
exit_conditions = [['pricevolcorr', '>', 0.5]]
timelimit = 'MAX30'  # MAXN or MINN, default to MIN0 if days don't play a role
stoploss = 'static4'  # staticN, trailingN for standard deviaton times N, default to None
target = 'trailing3'  # staticN, trailingN for standard deviaton times N, default to None

long_trades[performance_vars] = long_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(exit_conditions,'|'),timelimit,stoploss,target),axis=1)

printPerformance(long_trades)
performance = getOverallPerformance(long_trades)
s1 = saveStrategy("first","long",entryConditions,exit_conditions,timelimit,stoploss,target,performance)
printStrategy(s1)

short_trades = applyEntry(signalsdf,combine(entryConditions,'&'),subset_original_days,'short')
short_trades[performance_vars] = short_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(exit_conditions,'|'),timelimit,stoploss,target),axis=1)
printPerformance(short_trades)

performance = getOverallPerformance(short_trades)
s2 = saveStrategy("first","short",initialEntryConditions,exit_conditions,timelimit,stoploss,target,performance)
printStrategy(s2)



###### Decision Tree
def runDT(subset,treefilename):
    global conditions_list
    # On scored subset, decision tree can be run, but first,
    non_features = ['Total Trade Quantity', 'Low', 'Close', 'CloseAdjforDividend', 'Open', 'High', 'original',
                    'MACD', 'MACDsignal', 'MACDhist', 'RSI', 'daily_volatility', 'Prev_Close', 'ATR', 'Bollinger_High', 'Bollinger_Low',
                    'avgVol', 'pricevolcorr', 'log_daily_returns', 'longshort']
    all_features = [x for x in subset.keys() if ((x not in performance_vars) and (x not in non_features))]
    # Remove columns that were used for entry or exit
    # features_in_subset_conditions = getFeaturesfromConditions(subset_conditions)  # replace with some method of defining what features to use
    # features_in_exit_conditions = getFeaturesfromConditions(pre_dt_exit_conditions)
    # features_to_model = [x for x in all_features if ((x not in features_in_subset_conditions) and (x not in features_in_exit_conditions))]
    features_to_model = all_features
    # Drop continuous and nifty features
    # continuous_features = ['Volumevsavgvolume', 'Volumeincrease_last7days', 'nifty_volatility', 'volatility_by_niftyvolatility',
    #                        'volatilityincrease_last7days']
    continuous_features = []
    nifty_features = ['nifty_volatility', 'nifty_MACDgtSignal', 'nifty_MACDltSignal', 'nifty_MACDgt0', 'nifty_MACDlt0',
                      'volatility_by_niftyvolatility']
    features_to_model = [x for x in features_to_model if ((x not in continuous_features) and (x not in nifty_features))]
    to_model = subset.copy()
    # Remove columns that are not features

    to_model = to_model.reset_index()
    # test for missing features
    for key in features_to_model:
        a = to_model[key].isnull().sum()
        b = len(to_model[key])
        if a > 0:
            print(key + " is missing " + str(a) + " out of " + str(len(to_model[key])) + " values")
    # if too many, drop the column itself first
    to_model = to_model.drop(columns=[])
    # if not too many, drop the rows
    to_model.dropna(inplace=True)  # drop rows with missing values
    # Prepare datasets for modeling
    X = to_model[features_to_model]
    np.isnan(X).any()  # is all false
    y = to_model['winloss']
    # y = to_model['signal_return']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
    # for i in range(1,10,1):
    clf_gini = DecisionTreeClassifier(criterion="gini", random_state=10, max_depth=4, min_samples_leaf=50)
    clf_gini.fit(X_train, y_train)
    y_pred = clf_gini.predict(X_test)
    print(accuracy_score(y_test, y_pred) * 100)
    # visualize
    dot_data = StringIO()
    export_graphviz(clf_gini, out_file=dot_data, filled=True, feature_names=features_to_model)
    graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
    graph.write_png(treefilename)  # had to install Graphviz on laptop
    # convert to conditions
    conditions_list = getConditionsfromTree(clf_gini, features_to_model)

    return conditions_list

conditions_list = runDT(long_trades,'tree.png')

# to parse above output (iterating through a list of dicts)
for item in conditions_list:
    print(item['conditions'])
    print(item['samples'])
    print(item['winrate'])

# finding conditions with maximum and minimum winrate

conditions_with_max_winrate = max(conditions_list, key=lambda x:x['winrate'])['conditions']
conditions_with_min_winrate = min(conditions_list, key=lambda x:x['winrate'])['conditions']

######

newEntryConditions = initialEntryConditions + conditions_with_max_winrate

long_trades = applyEntry(signalsdf,combine(newEntryConditions,'&'),subset_original_days,'long')

long_trades[performance_vars] = long_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(exit_conditions,'|'),timelimit,stoploss,target),axis=1)
performance = getOverallPerformance(long_trades)
s3 = saveStrategy("second","long",newEntryConditions,exit_conditions,timelimit,stoploss,target,performance)
printStrategy(s3)
# save strategy to file
with open('strategy.json', 'w') as fp:
    json.dump(s3, fp)
# read strategy from file
with open('strategy.json') as json_data:
    s4 = json.load(json_data)

##### Have to figure out exit conditions that work here
features = getFeaturesfromConditions(newEntryConditions)
plot_random_signal(long_trades,features,signalsdf)

##### based on inspection - this is a strategy identifying sudden drops in price, so, use trailing target

# newEntryConditions = [['RSIlt30', '==', 1], ['volatilityrose_2weeksago', '<=', 0.5], ['Volumerose_2weeksago', '<=', 0.5], ['Volumerose_last7days', '<=', 0.5]]
newEntryConditions = [['RSI', '<', 20], ['volatilityrose_2weeksago', '<=', 0.5], ['Volumerose_2weeksago', '<=', 0.5]]

exit_conditions = [['RSI','>',50]]
timelimit = 'MAX10'  # MAXN or MINN, default to MIN0 if days don't play a role
stoploss = 'static2'  # staticN, trailingN for standard deviaton times N, default to None
target = 'trailing4'    # staticN, trailingN for standard deviaton times N, default to None

subset_original_days = 7

long_trades = applyEntry(signalsdf,combine(newEntryConditions,'&'),subset_original_days,'long')
long_trades[performance_vars] = long_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine(exit_conditions,'|'),timelimit,stoploss,target),axis=1)
performance = getOverallPerformance(long_trades)

s5 = saveStrategy("RSIlowtoRSIhigh","long",initialEntryConditions,exit_conditions,timelimit,stoploss,target,performance)
filename = 'strategies' + '.json'
complete_name = os.path.join('outputs','strategies'+'.json')
with open(complete_name, 'w') as fp:
    json.dump(strategy, fp)

conditions_list = runDT(long_trades,'tree.png')



name = 'RSIlowTrailingST'
# of signals =    1155  Winrate =  40.6%  Avg return =  0.94%  Avg days of holding =  14.7  Avg win return =  7.66%  Avg lose return =  -3.73%
name = 'RSIlowtoRSIhigh'
# of signals =     287  Winrate =  73.5%  Avg return =  6.57%  Avg days of holding =  64.3  Avg win return =  11.83%  Avg lose return =  -9.53%
filename = name + '.json'
with open(filename) as json_data:
    strat = json.load(json_data)


# correlation of features to target
# X.apply(lambda x: x.corr(y))



####################### Bivariate analysis to see if a variable distinguishes winloss % ################

from candlestick import plot_bivariate_continuous
from candlestick import plot_bivariate

# Categorical bivariate plotting - basically, does a variable, or two, differentiate between win %
vars_to_bivariate = ['MACDhigherthansignal',
       'MACDlowerthansignal','MACD+vecrossoverinlast3days', 'MACD-vecrossoverinlast3days',
       'MACDrose2weeksago', 'MACDfell2weeksago']
plot_bivariate(signalsdf, vars_to_bivariate)  # creates several bivariate plots against winloss %
new_signalsdf = signalsdf[signalsdf['MACDroselastweek']==1]
plot_bivariate(signalsdf,['MACDroselastweek'])
plot_bivariate(signalsdf,['Volumeroselastweek'])
plot_bivariate(new_signalsdf,['Volumeroselastweek'])
plot_bivariate(new_signalsdf,['pricevolcorr+ve'])

#plot a bivariate of a continuous var

plot_bivariate_continuous(new_signalsdf,'pricevolcorr',8)
plot_bivariate_continuous(new_signalsdf,'volumevsavgvolume',10)
plot_bivariate_continuous(new_signalsdf,'volumeincreaseinlast7days',5)
plot_bivariate_continuous(new_signalsdf,'beta_sortof',5)
plot_bivariate_continuous(new_signalsdf,'RSI',3)


