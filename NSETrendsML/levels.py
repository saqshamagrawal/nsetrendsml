
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
# from sklearn.preprocessing import MinMaxScaler
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
# import matplotlib.dates as mdates
import candlestick  # this contains the function to plot stockdata

import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')
# signalsdf = pd.read_pickle('signalsdf.pickle')  # reading from file
# signalsdf.signal_return = signalsdf.signal_return/100  ################ ONLY FOR CURRENT PICKLE ##################

# How to get to a ticker from the unpickled dataframe
# for ticker in signalsdf.index.levels[0]:
#     print(ticker)

# Picking a random stock from the unpickled dataframe
# import random
# stockdata = signalsdf.loc[random.choice(signalsdf.index.levels[0])]
# stockdata_last_year = stockdata[-200:]
# stockdata = stockdata[-200:]

"""
Basically the below code
1. Plots the stockdata and its volume
2. On the same figure, plots the 'levels' (and prints a list) for each of 200, 100 and 50 days of data
3a. For this, it selects the top 30% of the days with largest volumes
3b. breaks the range into 50 bins (in the function form, bins are calculated in advance for all three iterations
3c. creates histograms of Lows and Highs (as supports and resistances)
3d. Picks the one (or more) levels with the highest value in the histogram

Seems to work very well for about half the samples, and reasonable for another 30%. For 20%-odd, not as accurate
Will need to be figured out how to actually use (probably in setting stoploss and target on visual inspection)

"""

# function to get levels with specific num_of_days and bins
# is called by the function get_levels
def get_levels_with_bins(stockdata, num_of_days, bins, vol_perc_cutoff):
    levels = []
    # pick the last num_of_days days
    stockdata = stockdata[-1*num_of_days:]
    # Find highest volume days
    volume_cutoff = np.percentile(stockdata['Total Trade Quantity'].dropna(), vol_perc_cutoff)  # 90th (or some) percentile of volume
    # which percentile to take - basically, too high a cutoff loses information, and too low a cutoff doesn't remove any noise
    # something between 50 and 80 seems like a good compromise
    stockdata_highvolume = stockdata[stockdata['Total Trade Quantity'] > volume_cutoff]
    hist, bins = np.histogram(stockdata_highvolume.Close.values, bins=bins)  # use the bins passed as an argument
    # This parameter basically suggests how precise do we need to be in identifying levels - turns out, visually
    # we do not need to (and cannot) be very precise anyway
    hist_support, bins = np.histogram(stockdata_highvolume.Low.values, bins=bins)  # create a histogram of support points
    hist_resist, bins = np.histogram(stockdata_highvolume.High.values, bins=bins)  # create a histogram of resistances
    # levels = bins[np.append(hist_support+hist_resist>10,False)]  # finally, add the two histograms (not ideal I suppose)
    # And this cutoff is arbitrary as well, and must be greater if the volume percentile is lower or bins are fewer
    # levels = bins[np.append(hist_support+hist_resist == max(hist_support+hist_resist),False)]
    levels = np.append(bins[np.append(hist_support == max(hist_support), False)], bins[np.append(hist_resist == max(hist_resist), False)])
    return levels  # numpy array

# function to get levels of overall stockdata (50, 100, 200 days)
def get_levels(stockdata):
    # Should first subset the stockdata to last 200 days or something, before calling the function or here
    # identify some arbitrary bins of the stock price range - here, by creating the histogram of the stockdata
    hist, bins = np.histogram(stockdata.Close.values, bins=50)  # consider using pandas.cut
    # Possible exploration - maybe bins dont need to be all equal in size
    levels_200 = get_levels_with_bins(stockdata, 200, bins, 70)  # usually 2 but sometimes more in a numpy array
    levels_100 = get_levels_with_bins(stockdata, 100, bins, 70)  # at least one each of support and resistance
    levels_50 = get_levels_with_bins(stockdata, 50, bins, 70)
    levels = {50: levels_50, 100: levels_100, 200: levels_200}  # creating a dict using {}
    return levels

# function to get levels of overall stockdata (50, 100, 200 days)
def get_levels_new(stockdata):
    # Should first subset the stockdata to last 200 days or something, before calling the function or here
    # identify some arbitrary bins of the stock price range - here, by creating the histogram of the stockdata
    hist, bins = np.histogram(stockdata.Close.values, bins=10)  # consider using pandas.cut
    # Possible exploration - maybe bins dont need to be all equal in size
    levels_90 = get_levels_with_bins(stockdata, 90, bins, 70)  # usually 2 but sometimes more in a numpy array
    levels_60 = get_levels_with_bins(stockdata, 60, bins, 70)  # at least one each of support and resistance
    levels_30 = get_levels_with_bins(stockdata, 30, bins, 70)
    levels = {30: levels_30, 60: levels_60, 200: levels_90}  # creating a dict using {}
    return levels

# function to plot the levels on a price-time axis
def plot_level(ax, levels):
    # levels are dicts of lists, with key being number of days (50, 100, 200), and lists of price levels
    leftmost, rightmost = ax.get_xlim()
    print(rightmost-leftmost)
    for num_of_days, level_list in levels.items():
        for level in level_list:
            # ax.plot([rightmost - num_of_days, rightmost], [level, level], 'g-',linestyle='--',linewidth=0.5)
            leftpoint = leftmost - num_of_days
            ax.plot([leftpoint, rightmost], [level, level], color=plt.cm.RdYlBu(num_of_days),linestyle='--',linewidth=1)


# # Plot the stockdata
# fig, ax = plt.subplots(nrows=2,ncols=1,sharex='col')  # make it the same x axis on all three
# candlestick.plot_candlestick(ax[0], stockdata)
# candlestick.plot_volume(ax[1], stockdata)
#
# levels = get_levels(stockdata_last_year)
# print(levels)
# plot_level(ax[0], levels)


"""
# Problems and opportunities of current approach:

# The initial choice of "possible levels" is arbitrary, since we bin the whole range into 10, 20 etc. bins.
# So, the histogram approach works - esp to identify which of the lines are NOT significant levels, but it lose sight of
# some of the levels that stay for less amount of time
# And it might be difficult to set these three (or two, assuming 1st one 70%) parameters algorithmically

# Somehow we have to reject levels that are close by but high in histogram, in favor of levels that are farther apart
# But might not have as many datapoints to add up in the histogram (e.g. global maximum
# Will a simple moving average do the job?

"""

"""
Thoughts for how to get to some useful levels

lows indicate support, highs indicate resistance - but in the end, both are just "levels"
hi volume days should be more siginificant (say 75 percentile of volume or more
Local maxima and minima can be found
MACD or some sort of smoothening will remove local noise and provide points that are genuine local maxima / minima
Separating recent from old could be a good thing or a bad thing
levels only of last year is relevant
Some sort of band (e.g., breaking one year range into hundred parts
One or more local maxima for resistance (minima for support) in the same "band"
Band should be similar in size to "buy" and sell range - 0.5 times current SD or some such

How will we use the levels later?

"""


# Not used - function using k means to identify levels
def plot_stock_levels(stockdata, ax):

    stockdata_high = stockdata.High.values  # highs are resistances
    stockdata_low = stockdata.Low.values  # while lows are supports
    # stockdata.keys()
    # By using below "elbow method to judge the right "k", 3 seems reasonable enough.
    # sse = {}
    # for k in range(1, 10):
    #     kmeans = KMeans(n_clusters=k, max_iter=100).fit(nifty_high.reshape(-1, 1))
    #     sse[k] = kmeans.inertia_
    # plt.plot(sse.keys(),sse.values())
    num_clusters = 3  # max(3, np.mod(len(stockdata),50))

    kmeans = KMeans(n_clusters=num_clusters)
    # Fitting the price data
    kmeans = kmeans.fit(stockdata_high.reshape(-1, 1))

    centroids_high = kmeans.cluster_centers_[:, 0]  # notice the indexing (slicing) bcoz 1 dimension
    # centroids_high.sort()
    # print(centroids_high)
    kmeans = kmeans.fit(stockdata_low.reshape(-1, 1))
    centroids_low = kmeans.cluster_centers_[:, 0]
    # kmeans
    # centroids_low.sort()
    # print(centroids_low)
    min_low = np.min(stockdata.Low)
    max_high = np.max(stockdata.High)
    bins = np.concatenate((centroids_high, centroids_low))
    bins = np.append(bins, min_low)
    bins = np.append(bins, max_high)
    bins = np.unique(bins)
    bins.sort()
    # Maybe bins are not the cluster_centroids, but some other summary of Highs and Lows
    # if the candle is tall, green, then its low represents demand, corresponding to volume, recency, esp. if high is reinforced by close price
    #     and tall red, then the high represents supply, (+volume and recency) esp. if this value is reinforced by close
    stockdata['green'] = np.where(stockdata['Close'] > stockdata['Open'], 1, 0)
    stockdata['demand_price'] = stockdata['green'] * stockdata['Low']
    stockdata['demand_amount'] = stockdata['green'] * stockdata['Total Trade Quantity']
    stockdata['red'] = np.where(stockdata['Close'] > stockdata['Open'], 0, 1)
    stockdata['supply_price'] = stockdata['red'] * stockdata['High']
    stockdata['supply_amount'] = stockdata['red'] * stockdata['Total Trade Quantity']
    np.histogram(stockdata['supply_price'])
    # plt.hist(stockdata['supply_price'])
    # plt.scatter(stockdata['demand_price'],stockdata['demand_amount'])
    stockdata['demand_buckets'] = pd.cut(stockdata['demand_price'], bins=bins)
    stockdata['supply_buckets'] = pd.cut(stockdata['supply_price'], bins=bins)
    stockdata.groupby(['demand_buckets']).count()
    stockdata.groupby(['supply_buckets']).count()
    demand_by_price = stockdata.groupby(['demand_buckets'])['demand_amount'].sum()
    demand_datapoints = stockdata.groupby(['demand_buckets'])['green'].sum()
    supply_by_price = stockdata.groupby(['supply_buckets'])['supply_amount'].sum()
    supply_datapoints = stockdata.groupby(['supply_buckets'])['red'].sum()
    a = pd.concat([demand_by_price, supply_by_price, supply_datapoints, demand_datapoints], axis=1)

    print(a)
    # fig, ax = plt.subplots()
    # ax.plot(stockdata.Close)
    for x in centroids_high:
        ax.plot([stockdata.index.astype('O').min(), stockdata.index.astype('O').max()], [x, x], 'b-')
    for x in centroids_low:
        ax.plot([stockdata.index.astype('O').min(), stockdata.index.astype('O').max()], [x, x], 'r-')
# plot_stock_levels(stockdata)
# plot_stock_levels(stockdata_highvolume,ax[0])


