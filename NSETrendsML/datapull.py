
import quandl  # https://www.quandl.com/tools/python
from quandl.errors.quandl_error import NotFoundError
from time import sleep
import time
import pandas as pd
import json
import requests
from alpha_vantage.timeseries import TimeSeries
from snapi_py_client.snapi_bridge import StocknoteAPIPythonBridge

def pullNSEdatafromQuandl(tickerlist, benchmarklist, startdate, enddate):

    # Quandl is now a paid source, and database is different from what this code was written for
    # This source is still providing data till Jan 6, 2019, as of March 13, 2019

    # -------- NSE data from Quandl --------
    # Gives date, open, high, low, last, close, volume and turnover
    # does not account for splits :(
    # gets updated for a given day by about 6-7 pm IST of that day
    quandl.ApiConfig.api_key = 'ZQz3NejSsbsfNb3SFzYs'  # At sign-up they give a personal token
    # For example:
    # print(quandl.get("NSE/SBIN", authtoken="ZQz3NejSsbsfNb3SFzYs"))
    # sbidata = quandl.get("NSE/SBIN", authtoken="ZQz3NejSsbsfNb3SFzYs", start_date="2017-01-01", end_date="2018-02-02")

    datapull_start_time = time.time()

    NSEdata = {}

    for item in benchmarklist:

        benchmarkdata = quandl.get(item, start_date=startdate, end_date=enddate)
        print("pulling data for "+item)

        new_columns = benchmarkdata.columns.values
        new_columns[4] = 'Total Trade Quantity'
        benchmarkdata.columns = new_columns

        # benchmarkdata.rename(columns={'Shares Traded': 'Total Trade Quantity'})  # doesnt work
        NSEdata[item] = benchmarkdata

    for ticker in tickerlist:
        sleep(0.3)  # sleeping 0.3 seconds (sleep() uses seconds, but you can also use floats)
        print("pulling data for ticker " + ticker)
        tickerQ = ticker.replace("&", "").replace("-", "_")  # removing & for tickers like L&TFH and replacing - by _
        try:
            stockdata = quandl.get('NSE/' + tickerQ, start_date=startdate, end_date=enddate)
        except NotFoundError:
            print(NotFoundError)
            print("did not find the ticker " + tickerQ + " in Quandl NSE data; trying without special chars")
            try:
                tickerQ = ''.join(e for e in tickerQ if e.isalnum())  # removing all special characters
                stockdata = quandl.get('NSE/' + tickerQ, start_date=startdate, end_date=enddate)
            except NotFoundError:
                print("did not find the ticker  " + tickerQ + " in Quandl NSE data")

        NSEdata[ticker] = stockdata

    datapull_stop_time = time.time()
    print("data pull took", abs(datapull_stop_time - datapull_start_time), "seconds")
    return NSEdata



"""
Notes on puling data from Alpha Vantage
Risks
    UNRELIABLE, gives errors sometimes without clear pattern
    There's no documentation
    Cant specify start date, and end-date is assumed to be latest
    Errors are not handled, even by the alpha-vantage library
Speed of data pull
    Upto 5 API requests per minute and 500 requests per day - so much slower than Quandl
Close vs Adjusted Close
    "4. close" seems to be adjusted for splits, but not for dividends (e.g. TCS in March 2009 is 1/4th that of Quandl data)
    "5. adjusted close" seems to be adjusted for both splits and dividends (making it lower than "3. low" sometimes)
    For recent history (last 3 months, say), both are same
    For now, using "4. close" as the Close, because it accounts for splits, while also being between Low and High
Missing data
    All have intermittent 0s for 60-odd days between 2003 and 2015 
    For data from 2006 onwards, there are 6 missing values, which are present in Quandl data, these should be imputed
    NSEdatafromQuandl['HDFCBANK'].loc[b[b['Close'] == 0].index.values].Close
    For one date ('2012-11-11'), even Quandl has no data because it's a Sunday, so index can simply be removed
    Currently dropping these rows (usually 62)
The index is not datetime index, rather <class 'pandas.core.indexes.base.Index'>
Last date
    Gives most uptodate data at the time of pulling, so on 15th Mar, the close price is NOT the close on 15th Mar
    Rather, it is the LTP if the day is still in progress


The Good Things:
    Lot of developer support apparently; https://www.alpha-vantage.community/
    Adjusted prices
    Not just daily but other frequencies also available
    Not just nifty but other exchanges also available
    Not just equity but other asset classes also available

How to get nifty or other benchmarks? 
"""
#ouputsize now means no. of data points needed
def pullNSEdatafromAlphaVantage(tickerlist,outputsize):
    datapull_start_time = time.time()
    ts = TimeSeries(key='WLYN411OUANSFCB7', output_format='pandas', indexing_type='date')
    NSEdata = {}
    data_pulled = False
    for ticker in tickerlist:
        # sleep(12)  # sleeping 12 seconds (keeps throttle to <5 calls a minute) - really improves the function
        # print("pulling data for ticker " + ticker, end = " ")
        # retry_count = 0
        # try:
        #     # stockdata, meta_data = ts.get_daily_adjusted(symbol='NSE:' + ticker, outputsize='full')  # also works, maybe useful for indices?
        #     stockdata, meta_data = ts.get_daily_adjusted(symbol=ticker + '.NS', outputsize='full')
        #     print("pulled " + ticker +'.NS')
        # except (KeyError,ValueError):
        #     # this probably is a temporary problem, so retry with sleep
        #     # seems to throw either of these two errors, whether its because ticker is not present or just because something went wrong
        #     while retry_count <= 2:
        #         retry_count +=1
        #         print('retrying: retry count',retry_count,end=" ")
        #         sleep(12)  # sleeping 12 seconds (keeps throttle to <5 calls a minute)
        #         try:
        #             stockdata, meta_data = ts.get_daily_adjusted(symbol=ticker + '.NS', outputsize='full')
        #             print("pulled " + ticker + '.NS')
        #             break
        #         except KeyError:
        #             if retry_count == 2: print("not pulled, moving on")
        #             pass
        #     continue
        # except ValueError as e:
        #      print("data not pulled for ticker " + ticker)
        #      print(e)
        #      continue

        print("pulling data for ticker " + ticker)
        retry_count = 0
        while retry_count <= 2:
            sleep(12)  # sleeping 12 seconds (keeps throttle to <5 calls a minute)
            try:
                # stockdata, meta_data = ts.get_daily_adjusted(symbol='NSE:' + ticker, outputsize='full')  # also works, maybe useful for indices?
                # stockdata, meta_data = ts.get_daily_adjusted(symbol=ticker + '.NS', outputsize=outputsize)
                if outputsize <=100:
                    stockdata, meta_data = ts.get_daily_adjusted(symbol=ticker + '.NS', outputsize='compact')
                    if outputsize <= len(stockdata):
                        stockdata = stockdata[len(stockdata)-outputsize:]
                else:
                    stockdata, meta_data = ts.get_daily_adjusted(symbol=ticker + '.NS', outputsize='full')
                    if outputsize <= len(stockdata):
                        stockdata = stockdata[len(stockdata)-outputsize:]
                print("pulled " + ticker + '.NS')
                data_pulled = True
                retry_count = 10 # this should break the while loop (using break probably just breaks out of try:)
            except (KeyError, ValueError):
                data_pulled = False
                retry_count +=1
                print('retrying: retry count',retry_count)
                continue

        if data_pulled:
            # rename columns of stockdata to match earlier Quandl output
            stockdata = stockdata.rename(columns={"1. open": "Open", "2. high": "High", "3. low": "Low", "4. close": "Close",
                                          "5. adjusted close": "CloseAdjforDividend", "6. volume": "Total Trade Quantity"})
            stockdata = stockdata.drop(columns=['7. dividend amount','8. split coefficient'])
            stockdata.index = pd.to_datetime(stockdata.index)  # change index to datetime
            # Drop rows where Close values are 0
            # print(str(len(stockdata[stockdata.Close == 0]))+ " empty rows will be dropped out of " + str(len(stockdata) + " total"))
            stockdata = stockdata[stockdata.Close != 0]
            # print(stockdata.index.min())
            NSEdata[ticker] = stockdata

    datapull_stop_time = time.time()
    print("data pull took", abs(datapull_stop_time - datapull_start_time), "seconds")
    return NSEdata, datapull_stop_time

#copy pasting for now with few changes 
def pullHourlyNSEdatafromAlphaVantage(tickerlist,outputsize,interval = '60min'):
    datapull_start_time = time.time()
    ts = TimeSeries(key='WLYN411OUANSFCB7', output_format='pandas', indexing_type='date')
    NSEdata = {}
    data_pulled = False
    for ticker in tickerlist:
        print("pulling data for ticker " + ticker)
        retry_count = 0
        while retry_count <= 2:
            sleep(12)  # sleeping 12 seconds (keeps throttle to <5 calls a minute)
            try:
                if outputsize <=100:
                    stockdata, meta_data = ts.get_intraday(symbol=ticker + '.NS', outputsize='compact',interval = interval)
                    if outputsize <= len(stockdata):
                        stockdata = stockdata[len(stockdata)-outputsize:]
                else:
                    stockdata, meta_data = ts.get_intraday(symbol=ticker + '.NS', outputsize='full', interval = interval)
                    if outputsize <= len(stockdata):
                        stockdata = stockdata[len(stockdata)-outputsize:]
                print("pulled " + ticker + '.NS')
                data_pulled = True
                retry_count = 10 # this should break the while loop (using break probably just breaks out of try:)
            except (KeyError, ValueError):
                data_pulled = False
                retry_count +=1
                print('retrying: retry count',retry_count)
                continue

        if data_pulled:
            # rename columns of stockdata to match earlier Quandl output
            stockdata = stockdata.rename(columns={"1. open": "Open", "2. high": "High", "3. low": "Low", "4. close": "Close",
                                          "5. adjusted close": "CloseAdjforDividend", "6. volume": "Total Trade Quantity"})
            stockdata = stockdata.drop(columns=['7. dividend amount','8. split coefficient'])
            stockdata.index = pd.to_datetime(stockdata.index)  # change index to datetime
            # Drop rows where Close values are 0
            # print(str(len(stockdata[stockdata.Close == 0]))+ " empty rows will be dropped out of " + str(len(stockdata) + " total"))
            stockdata = stockdata[stockdata.Close != 0]
            # print(stockdata.index.min())
            NSEdata[ticker] = stockdata

    datapull_stop_time = time.time()
    print("data pull took", abs(datapull_stop_time - datapull_start_time), "seconds")
    return NSEdata, datapull_stop_time



# Gives historical data for 5 years only
# Fails for some tickers - GRUH, INFRATEL etc.

def pullNSEdatafromSAMCO(tickerlist, fromdate):

    samco = StocknoteAPIPythonBridge()
    login=samco.login(body={"userId":'DR39462','password':'MURkl2010$','yob':'1984'})
    # login=samco.login(body={"userId":'XYZ','password':'*_iSXrncGY9xtRK','yob':'1984'})
    login_JSON=json.loads(login)
    samco.set_session_token(sessionToken =login_JSON['sessionToken'])
    # Login Completed

    # print("Login details",login)
    print("login completed")

    headers = {
        'Accept': 'application/json',
        'x-session-token': login_JSON['sessionToken']
    }

    # # For tickers
    # r = requests.get('https://api.stocknote.com/history/candleData', params={
    #     'symbolName': 'RELIANCE', 'fromDate': '2015-10-11'
    # }, headers=headers)
    #
    # # For indices
    # r = requests.get('https://api.stocknote.com/history/indexCandleData', params={
    #     'indexName': 'NIFTY 50', 'fromDate': '2020-03-09'
    # }, headers=headers)

    NSEdata = {}

    for ticker in tickerlist:
        # ticker = 'INFRATEL'
        # fromdate = '2016-10-11'
        print("pulling data for ticker " + ticker)
        r = requests.get('https://api.stocknote.com/history/candleData', params={
            'symbolName': ticker, 'fromDate': fromdate
        }, headers=headers)
        if(r.json()['status'] == 'Failure'):
            print(ticker+' Failed')
            # break
        else:
            stockdata = pd.DataFrame.from_dict(r.json()['historicalCandleData'])

            stockdata.index = pd.to_datetime(stockdata['date'])  # change index to datetime
            stockdata = stockdata.rename(columns={"open": "Open", "high": "High", "low": "Low", "close": "Close",
                                                  "volume": "Total Trade Quantity"})
            stockdata = stockdata.drop(columns=['ltp'])
            # Drop rows where Close values are 0
            # print(str(len(stockdata[stockdata.Close == 0]))+ " empty rows will be dropped out of " + str(len(stockdata) + " total"))
            # stockdata = stockdata[stockdata.Close != 0]
            # print(stockdata.index.min())

            cols = ['Open', 'Low', 'High', 'Close', 'Total Trade Quantity']
            stockdata[cols] = stockdata[cols].apply(pd.to_numeric, errors='coerce', axis=1)  # since all datapoints come as strings
            NSEdata[ticker] = stockdata


    return NSEdata

