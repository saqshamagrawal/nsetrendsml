import numpy as np
import pandas as pd
from tqdm import tqdm  # for status bar
from sklearn.tree import _tree  # for getConditionsfromTree
from datetime import timedelta  # for scoreExit

def markoriginalsignals(signalsdf, num_of_days):
    x = signalsdf.reset_index()  # removing the multiindex
    # Sorting by ticker and then date (this is required before we call a signal duplicate)
    x = x.sort_values(['Ticker', 'Date'], axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    # define original / actionable ticker as different ticker or > 30d
    duplicate_condition = (x.Ticker != x.Ticker.shift(1)) | ((x.Date - x.Date.shift(1)).astype('timedelta64[D]') > num_of_days)
    # OR define original / actionable ticker as different ticker or > held for days of previous signal
    # duplicate_condition = (x.Ticker != x.Ticker.shift(1)) | ((x.Date - x.Date.shift(1)).astype('timedelta64[D]') > x.held_for_days.shift(1))
    x['original'] = np.where(duplicate_condition == True, 1, 0)
    return x.set_index(['Ticker', 'Date'])

def subset_originals(signalsdf, num_of_days):
    signalsdf_o = markoriginalsignals(signalsdf, num_of_days)
    signalsdf_o = signalsdf_o[signalsdf_o['original'] == 1]
    return signalsdf_o

def combine(conditions, connector):
    # takes conditions in form of list of list
    # and connector of the form "&", "|", "and", "or"
    # and combines to give an expression that can be evaluated to decide entry or exit
    conditional_statement = ""
    num_connectors = len(conditions) - 1
    counter = 1
    if conditions == [[]]:
        pass
    else:
        for condition in conditions:
            conditional_statement += '(' + condition[0] + ' ' + condition[1] + ' ' + str(condition[2]) + ')'  # converts each condition to string
            if counter <= num_connectors:
                conditional_statement += ' ' + connector + ' '
                counter += 1
    return conditional_statement

def getFeaturesfromConditions(conditions):
    feature_list = []
    if conditions != [[]]:
        for condition in conditions:
            feature_list.append(condition[0])
    return feature_list

def applyEntry(signalsdf, entryCondition, subset_original_days, longshort):
    subset = signalsdf[signalsdf.eval(entryCondition)]
    print("signals selected",len(subset))
    subset_original = subset_originals(subset, subset_original_days)
    print("signals selected after removing duplicates",len(subset_original))
    subset_original['longshort'] = longshort
    return subset_original

def scoreExit(signal, NSEdatawithfeatures, exitCondition, days_to_hold, stoploss_type, target_type):
    # For Testing the function
    # signal = signalsdf.loc['VEDL', datetime.datetime.strptime('2018-12-06', '%Y-%m-%d')]
    # or # signal = signalsdf.loc['AXISBANK','2016-12-06']
    # exitCondition = '(RSI > 70)'
    # days_to_hold = 'MIN0'
    # stoploss_type = 'trailing3'
    # target_type = 'static4'
    # signal['longshort'] = 'short'

    print(signal.name)
    ticker = signal.name[0]
    signal_date = signal.name[1]
    futuredata = NSEdatawithfeatures[ticker][signal_date:]  # NSE data of that ticker after the signal date
    longshort = -1 if signal.longshort == 'short' else 1  # so longshort is 1 if long, -1 if short

    buy_price = longshort * signal.Close  # Assume that we buy at yesterday's close. This assumption will change
    sell_price = np.NaN
    signal_return = np.NaN
    held_for_days = np.NaN
    winloss0 = np.NaN
    winloss2 = np.NaN

    stoploss = float('-inf')
    target = float('+inf')
    possible_exits = pd.DataFrame(columns=futuredata.keys())  # empty dataframe

    # Default value of days_to_hold is MIN0; this value means days_to_hold has no impact whatsoever
    # Subset the futuredata based on days_to_hold value
    if days_to_hold[0:3] == 'MIN':
        futuredata = futuredata[signal_date + timedelta(int(days_to_hold[3:])):]  # take data after min days
    if days_to_hold[0:3] == 'MAX':
        planned_max_exit_date = signal_date + timedelta(int(days_to_hold[3:]))
        if planned_max_exit_date <= futuredata.index.max():
            actual_max_exit_date = futuredata[futuredata.index >= planned_max_exit_date].index.min()  # finds smallest date >= planned_max_exit_date
            futuredata = futuredata[:actual_max_exit_date]  # take data only up till max days
            possible_exits = possible_exits.append(futuredata.loc[actual_max_exit_date], sort=False)

    # Next, deal with conditions provided, if any
    if exitCondition:
        futuredata['exitConditionmet'] = futuredata.eval(exitCondition)
        conditional_exits = futuredata[futuredata['exitConditionmet']]  # this gives the rows where exitConditionmet
        # print(str(len(conditional_exits))+" conditional exits in futuredata")
        possible_exits = possible_exits.append(conditional_exits)

    # It's similar to deal with static stoploss and/or target
    if stoploss_type[0:6] == 'static':
        stoploss = signal.Close * (1 - longshort * int(stoploss_type[-1]) * signal.daily_volatility)  # for short, longshort is -1, so stoploss is higher than price
        # stoploss_exits = futuredata[futuredata.Close < stoploss]  # for long-only, this would work
        stoploss_exits = futuredata[longshort*futuredata.Close < longshort*stoploss]   # multiplying both by longshort helps preserve the <
        possible_exits = possible_exits.append(stoploss_exits, sort=False)

    if target_type[0:6] == 'static':
        target = signal.Close * (1 + longshort * int(target_type[-1]) * signal.daily_volatility)  # for short, longshort is -1, so stoploss is higher than price
        # target_exits = futuredata[futuredata.Close > target]  # for long-only, this would work
        target_exits = futuredata[longshort * futuredata.Close > longshort * target]  # multiplying both by longshort helps preserve the >
        possible_exits = possible_exits.append(target_exits, sort=False)

    # If instead we have a trailing stoploss and/or target, we got to loop through each day
    if (stoploss_type[0:8] == 'trailing') | (target_type[0:8] == 'trailing'):
        if stoploss_type[0:8] == 'trailing': stoploss = signal.Close * (1 - longshort * int(stoploss_type[8:]) * signal.daily_volatility)
        if target_type[0:8] == 'trailing': target = signal.Close * (1 + longshort * int(target_type[8:]) * signal.daily_volatility)
        for date, features in futuredata.iterrows():
            if longshort * features.Close > longshort * target or longshort * features.Close < longshort * stoploss:
                possible_exits = possible_exits.append(futuredata.loc[date], sort=False)  # this row becomes a candidate
                break
            else:
                # Implementing trailing stoploss / target for LONG & SHORT signals (multiplying by longshort helps preserve max
                if stoploss_type[0:8] == 'trailing': stoploss = longshort * max(longshort * stoploss, longshort * features.Close * (1 - longshort * int(stoploss_type[8:]) * features.daily_volatility))
                if target_type[0:8] == 'trailing': target = longshort * max(longshort * target, longshort * features.Close * (1 + int(target_type[8:]) * features.daily_volatility))

    # Finally we find the minimum date among the we found above and the features at that date
    if not possible_exits.empty:
        # exit_row = futuredata.loc[possible_exits.index.min()]  # can give more than one rows (identical)
        # instead just sort by date and take the first row
        exit_row = possible_exits.sort_index().iloc[0]
        # And if we don't find any value, then if it was Max, we got to return the max date, else we return nothing
        sell_price = longshort * exit_row.Close  # Assumes that we sell at yesterday's close. This assumption will change
        signal_return = longshort * (sell_price - buy_price) / buy_price
        held_for_days = (exit_row.name - signal_date).days

    # print(buy_price, sell_price, held_for_days, signal_return, stoploss, target)
    winloss = 1 if signal_return > 0 else 0
    winloss2 = 1 if signal_return > 0.02 else 0
    return pd.Series([buy_price, sell_price, signal_return, held_for_days, winloss, winloss2], index=['buy_price','sell_price','signal_return','held_for_days', 'winloss', 'winloss2'])

def printPerformance(signalsdf):
    print("# of signals = ", "{:>6}".format(len(signalsdf)), end="  ")
    print("Winrate = ","{:.1%}".format(signalsdf.winloss.mean()), end="  ")
    print("Avg return = ","{:.2%}".format(signalsdf.signal_return.mean()), end="  ")
    print("Avg days of holding = ","{:.1F}".format(signalsdf.held_for_days.mean()), end="  ")
    print("Avg win return = ","{:.2%}".format(signalsdf[signalsdf.winloss == 1].signal_return.mean()), end="  ")
    print("Avg lose return = ","{:.2%}".format(signalsdf[signalsdf.winloss == 0].signal_return.mean()), end="  ")
    print("Std dev return = ","{:.2F}".format(signalsdf.signal_return.std()), end="  ")
    print("Skew return = ","{:.2F}".format(signalsdf.signal_return.skew()), end="  ")
    print("Kurtosis return = ","{:.2F}".format(signalsdf.signal_return.kurt()))

def getOverallPerformance(signalsdf):
    performance = {"num_signals": len(signalsdf),
                   "winrate": signalsdf.winloss.mean(),
                   "avg_return": signalsdf.signal_return.mean(),
                   "avg_holding_days": signalsdf.held_for_days.mean(),
                   "avg_win_return": signalsdf[signalsdf.winloss == 1].signal_return.mean(),
                   "avg_lose_return": signalsdf[signalsdf.winloss == 0].signal_return.mean(),
                   "std_return": signalsdf.signal_return.std(),
                   "skew_return": signalsdf.signal_return.skew(),
                   "kurt_return": signalsdf.signal_return.kurt()}
    printPerformance(signalsdf)
    return performance

def saveStrategy(name, longshort, entry_conditions, exit_conditions, timelimit, stoploss, target, performance):
    strategy = {'name': name, 'longshort': longshort,
                'entry': entry_conditions,
                'exit': exit_conditions,
                'timelimit': timelimit,
                'stoploss': stoploss,
                'target': target,
                'performance': performance
                }
    return strategy

def printStrategy(strategy):
    print('name: ', strategy['name'])
    print('longshort: ', strategy['longshort'])
    print('entry: ', combine(strategy['entry'],'&'))
    print('exit: ', combine(strategy['exit'],'|'))
    print('timelimit: ', strategy['timelimit'])
    print('stoploss: ', strategy['stoploss'])
    print('target: ', strategy['target'])

    performance = strategy['performance']
    print("# of signals = ", "{:>6}".format(performance['num_signals']), end="  ")
    print("Winrate = ","{:.1%}".format(performance['winrate']), end="  ")
    print("Avg return = ","{:.2%}".format(performance['avg_return']), end="  ")
    print("Avg days of holding = ","{:.1F}".format(performance['avg_holding_days']), end="  ")
    print("Avg win return = ","{:.2%}".format(performance['avg_win_return']), end="  ")
    print("Avg lose return = ","{:.2%}".format(performance['avg_lose_return']))

def get_daily_pnl(signalsdf,NSEdatawithfeatures,starting_cash,cash_per_trade):
    signalsdf = signalsdf.reset_index(level=['Ticker'])  # change index to only Date
    signalsdf['entry_date'] = signalsdf.index
    signalsdf['exit_date'] = signalsdf['entry_date'] + pd.to_timedelta(signalsdf['held_for_days'].values,'d')
    PnL = pd.DataFrame(NSEdatawithfeatures['RELIANCE'].Close) * starting_cash / NSEdatawithfeatures['RELIANCE'].Close[0]
    # USED RELIANCE INSTEAD OF NIFTY BECAUSE NIFTY NOT AVAILABLE
    PnL = PnL.rename(columns={"Close": "nifty"})
    PnL['cashPrincipal'] = starting_cash
    PnL['realizedProfit'] = 0
    PnL['unrealizedProfit'] = 0
    PnL['exposedPrincipal'] = 0

    signalsdf.sort_values(by='Date')  # not important to sort unless cash_per_trade is dependent on cash
    for index,signal in tqdm(signalsdf.iterrows()):
        # print(signal.Ticker,signal.entry_date,signal.exit_date)
        PnL['cashPrincipal'][signal.entry_date:signal.exit_date] -= cash_per_trade        # remove from cash
        units = cash_per_trade / signal.buy_price
        PnL['realizedProfit'][signal.exit_date:] += units*(signal.sell_price - signal.buy_price)  # add to profit
        MTM_signal = NSEdatawithfeatures[signal.Ticker][signal.entry_date:signal.exit_date].Close * units
        try:
            PnL['unrealizedProfit'][signal.entry_date:signal.exit_date] += MTM_signal - cash_per_trade
        except ValueError:
            print(signal.Ticker+" signal doesn't have same length as index")
            if len(MTM_signal) > len(PnL['unrealizedProfit'][signal.entry_date:signal.exit_date]):
                print(signal.Ticker+" signal appears to have more values than index")
                contained = MTM_signal.index.isin(PnL['unrealizedProfit'][signal.entry_date:signal.exit_date].index)
                PnL['unrealizedProfit'][signal.entry_date:signal.exit_date] += MTM_signal[contained] - cash_per_trade
            else:
                print(signal.Ticker+" signal appears to have less values than index")
                contained = PnL['unrealizedProfit'][signal.entry_date:signal.exit_date].index.isin(MTM_signal.index)
                PnL['unrealizedProfit'][signal.entry_date:signal.exit_date][contained] += MTM_signal - cash_per_trade

    PnL['profit'] = PnL['unrealizedProfit'] + PnL['realizedProfit']
    PnL['exposedPrincipal'] = starting_cash - PnL['cashPrincipal']
    PnL['portfolio_value'] = starting_cash + PnL['profit']
    PnL['exposure'] = PnL['unrealizedProfit'] + PnL['exposedPrincipal']
    return PnL

def get_monthly_performance_from_pnl(PnL):
    # Sort by Date
    PnL = PnL.sort_values(['Date'], axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    grouped_by_month = PnL.groupby(pd.Grouper(freq='M'))  # Group by Months (month ending)
    monthly_performance = grouped_by_month.aggregate(
        {'exposure': {'Avg Exposure': 'mean'}, 'daily_return': {'Monthly Return': 'sum'}, 'nifty_daily_return': {'Monthly Nifty Return': 'sum'}})
    monthly_performance.columns = monthly_performance.columns.droplevel(0)
    # Not accurate because it adds up returns rather than multiply (1 + returns)
    return monthly_performance

def get_monthly_performance(signalsdf):
    # Sort by Date and then Ticker
    signalsdf = signalsdf.sort_values(['Date', 'Ticker'], axis=0, ascending=True, inplace=False, kind='quicksort',
                                      na_position='last')
    signalsdf = signalsdf.reset_index(level=['Ticker'])  # change index to only Date
    grouped_by_month = signalsdf.groupby(pd.Grouper(freq='M'))  # Group by Months (month ending)
    monthly_performance = grouped_by_month.aggregate(
        {'Ticker': {'Signals': 'count'}, 'signal_return': {'Avg Return': 'mean'}, 'winloss': {'Win Rate': 'mean'}})
    monthly_performance.columns = monthly_performance.columns.droplevel(0)
    return monthly_performance

# Below version was 100 times faster!!! (but has fewer outputs)
def get_pnl_fast(signalsdf,NSEdatawithfeatures,starting_cash,cash_per_trade):
    signalsdf = signalsdf.reset_index(level=['Ticker'])  # change index to only Date
    signalsdf['entry_date'] = signalsdf.index
    signalsdf['exit_date'] = signalsdf['entry_date'] + pd.to_timedelta(signalsdf['held_for_days'].values,'d')
    PnL = pd.DataFrame(NSEdatawithfeatures['NSE/CNX_NIFTY'].Close) * starting_cash / NSEdatawithfeatures['NSE/CNX_NIFTY'].Close[0]
    PnL = PnL.rename(columns={"Close": "nifty"})
    PnL['Cash'] = starting_cash
    PnL['MTM'] = 0
    signalsdf.sort_values(by='Date')  # not important to sort unless cash_per_trade is dependent on cash
    for index,signal in tqdm(signalsdf.iterrows()):
        # print(signal.Ticker,signal.entry_date,signal.exit_date)
        PnL['Cash'][signal.entry_date:] -= cash_per_trade           # remove from cash
        units = cash_per_trade / signal.buy_price                   # find out units
        PnL['Cash'][signal.exit_date:] += units*signal.sell_price   # add to cash
        MTM_signal = NSEdatawithfeatures[signal.Ticker][signal.entry_date:signal.exit_date].Close * units
        try:
            PnL['MTM'][signal.entry_date:signal.exit_date] += MTM_signal
        except ValueError:
            print(signal.Ticker+" signal doesn't have same length as index")
            if len(MTM_signal) > len(PnL['MTM'][signal.entry_date:signal.exit_date]):
                print(signal.Ticker+" signal appears to have more values than index")
                contained = MTM_signal.index.isin(PnL['MTM'][signal.entry_date:signal.exit_date].index)
                PnL['MTM'][signal.entry_date:signal.exit_date] += MTM_signal[contained]
            else:
                print(signal.Ticker+" signal appears to have less values than index")
                contained = PnL['MTM'][signal.entry_date:signal.exit_date].index.isin(MTM_signal.index)
                PnL['MTM'][signal.entry_date:signal.exit_date][contained] += MTM_signal
    PnL['portfolio_value'] = PnL['Cash'] + PnL['MTM']
    return PnL


def getConditionsfromTree(tree, feature_names):

    """
    Below function to take a decision tree and return a dict with conditions, samples and winrate
    Function became very complicated, therefore some notes
    1. Uses recursion
    2. Uses nested functions
    3. Inner function appends to a list initialized in the outer function
    4. Figuring out the outer return was tricky - now its a list of dicts containing list of conditions, and their sample size and winrate
    5. Figuring out inner return was also tricky, because it needs to be parsed at the beginning of inner function
    6. Most tricky - copy() was required because without it, the list keeps only references of previous executions and hence gets overwritten
    """

    tree_ = tree.tree_
    feature_name = [
        feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]

    conditions_list = []  # initialize a list (will contain dicts in it of the kind {'conditions': , 'samples': , 'winrate': })

    def recurse(node, depth, conditions):
        # conditions is a dict of the kind {'conditions': , 'samples': , 'winrate': }
        only_conditions = conditions['conditions'].copy()  # need to parse the dict to get the 'conditions' element
                                                            # , and COPY() ensures it's values & not reference
        if tree_.feature[node] != _tree.TREE_UNDEFINED:

            samples = tree_.value[node].item(0) + tree_.value[node].item(1)
            winrate = tree_.value[node].item(1) / samples
            conditions['samples'] = samples
            conditions['winrate'] = winrate

            # print("before append")
            # print(conditions_list)
            # print(conditions)
            conditions_list.append(conditions.copy())  # COPY  # append to a variable outside the scope of nested function

            # print("after append")
            # print(conditions_list)

            name = feature_name[node]
            threshold = tree_.threshold[node]

            if tree_.children_left[node]:
                only_conditions.append([name, '<=', threshold])
                conditions['conditions'] = only_conditions.copy()  # COPY
                conditions = recurse(tree_.children_left[node], depth + 1, conditions.copy())

            if tree_.children_right[node]:
                del only_conditions[depth-1:]  # this is a key step, which removes the "<=" condition when moving from left child to right child
                only_conditions.append([name, '>', threshold])
                conditions['conditions'] = only_conditions.copy()  # COPY
                conditions = recurse(tree_.children_right[node], depth + 1, conditions.copy())
        else:
            samples = tree_.value[node].item(0) + tree_.value[node].item(1)
            winrate = tree_.value[node].item(1) / samples
            conditions['samples'] = samples
            conditions['winrate'] = winrate
            conditions_list.append(conditions.copy())  # COPY

        return conditions  # returns a dict of the kind {'conditions': , 'samples': , 'winrate': }

    recurse(0, 1, {'conditions': []})

    return conditions_list



