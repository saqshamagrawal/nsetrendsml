import pandas as pd
import numpy as np
import datetime

def gettechnicals(stockdata, averaging_period):

    """
    Available technicals:
    MACD
    MACDsignal
    MACDhist
    RSI
    daily_volatility
    ATR
    Bollinger_High (3 standard deviations)
    Bollinger_Low (3 standard deviations)
    avgVol
    pricevolcorr
    log_daily_returns
    """

    # averaging_period is used as the "last X days of average" for RSI and other indicators, usually 14

    stockdata_df = pd.DataFrame(stockdata)   # a pandas DataFrame is more like a 2D matrix than a numpy array somehow

    # MACD (12-day mean minus 26-day mean, with a 9 day signal)
    stockdata_df['MACD245218'], stockdata_df['MACD245218_signal'], stockdata_df['MACD245218_hist'] = getMACD(stockdata_df,24,52,18)
    stockdata_df['MACD12269'], stockdata_df['MACD12269_signal'], stockdata_df['MACD12269_hist'] = getMACD(stockdata_df,12,26,9)
    stockdata_df['MACD6135'], stockdata_df['MACD6135_signal'], stockdata_df['MACD6135_hist'] = getMACD(stockdata_df,6,13,5)
    stockdata_df['MACD362'], stockdata_df['MACD362_signal'], stockdata_df['MACD362_hist'] = getMACD(stockdata_df,3,6,2)

    # Relative Strength Index (RSI) (average over lookback period)

    delta = stockdata_df.Close.diff().dropna()
    u = delta * 0
    d = u.copy()
    u[delta > 0] = delta[delta > 0]  # take only +ve moves
    d[delta < 0] = -delta[delta < 0]  # take only -ve moves
    # copy-pasted - not sure how this works
    # u[u.index[period-1]] = np.mean(u[:averaging_period])  # first value is sum of avg gains
    # u = u.drop(u.index[:(period-1)])
    # d[d.index[period-1]] = np.mean(d[:averaging_period])  # first value is sum of avg losses
    # d = d.drop(d.index[:(period-1)])

    # Note on Apr 4, 2019 - Above, it was taking a period (which used to be 9 because of MACD
    # Below, we have changed period to averaging_period which is usually 14

    u[u.index[averaging_period-1]] = np.mean(u[:averaging_period])  # first value is sum of avg gains
    u = u.drop(u.index[:(averaging_period-1)])
    d[d.index[averaging_period-1]] = np.mean(d[:averaging_period])  # first value is sum of avg losses
    d = d.drop(d.index[:(averaging_period-1)])

    umean = u.ewm(ignore_na=False, min_periods=0, com=averaging_period - 1, adjust=False).mean()
    dmean = d.ewm(ignore_na=False, min_periods=0, com=averaging_period - 1, adjust=False).mean()
    # notice the use of com instead of span

    rs = umean/dmean  # ratio of ups vs. downs

    stockdata_df['RSI'] = 100 - 100 / (1 + rs)

    # Volatility metric - SDLPR Standard-dev of returns (measured as log price ratios)

    log_daily_returns = np.log(stockdata_df.Close / stockdata_df.Close.shift(1))
    # stockdata_df['SDLPR'] = log_daily_returns.rolling(window=averaging_period).std() * np.sqrt(averaging_period)
    stockdata_df['daily_volatility'] = log_daily_returns.rolling(window=averaging_period).std()
    # "Rolling" Standard Deviation of Log Price Ratios over averaging_period number of days

    # Average True Range (averaged over averaging_period)

    stockdata_df['Prev_Close'] = stockdata_df.Close.shift(1)

    range = stockdata_df.High - stockdata_df.Low   # always positive
    # upper_shadow = stockdata_df.High - stockdata_df.Close  # always positive
    # lower_shadow = stockdata_df.Low - stockdata_df.Close  # always positive
    # daily_return = stockdata_df.Close - stockdata_df.Prev_Close
    # overday_return = stockdata_df.Close - stockdata_df.Open
    # overnight_return = stockdata_df.Open - stockdata_df.Prev_Close
    highminuspreviousclose = abs(stockdata_df.High - stockdata_df.Prev_Close)  # So this is - abs(upper shadow + Return)
    lowminuspreviousclose = abs(stockdata_df.Low - stockdata_df.Prev_Close)    # and this is - lower shadow + Return

    true_range = pd.concat([range,highminuspreviousclose,lowminuspreviousclose],axis=1).min(axis=1)
    stockdata_df['ATR'] = true_range.rolling(window=averaging_period).mean()

    # Bollinger bands since easy to create
    no_of_std = 2
    rolling_mean = stockdata_df['Close'].rolling(averaging_period).mean()
    rolling_std = stockdata_df['Close'].rolling(averaging_period).std()
    stockdata_df['Bollinger_High'] = rolling_mean + (rolling_std * no_of_std)
    stockdata_df['Bollinger_Low'] = rolling_mean - (rolling_std * no_of_std)

    # Volume indicator - average volume over the averaging_period
    stockdata_df['avgVol'] = stockdata_df['Total Trade Quantity'].rolling(window = averaging_period).mean()

    # Rolling correlation between price and volume
    stockdata_df['pricevolcorr'] = stockdata_df['Total Trade Quantity'].rolling(window=averaging_period).corr(stockdata_df['Close'])

    # one day (yesterday) return
    stockdata_df['log_daily_returns'] = log_daily_returns

    # Clipping off the initial part which doesn't have accurate technicals
    stockdata_df = stockdata_df.loc[stockdata_df.index.min() + datetime.timedelta(52):]
    return stockdata_df


def getMACD(stockdata_df, fast, slow, period):
    SlowMA = stockdata_df.Close.ewm(ignore_na=False, min_periods=0, span=slow, adjust=True).mean()
    FastMA = stockdata_df.Close.ewm(ignore_na=False, min_periods=0, span=fast, adjust=True).mean()
    stockdata_df['MACD'] = FastMA - SlowMA
    stockdata_df['MACDsignal'] = stockdata_df.MACD.ewm(ignore_na=False, min_periods=0, span=period, adjust=True).mean()
    stockdata_df['MACDhist'] = stockdata_df.MACD - stockdata_df.MACDsignal
    return stockdata_df['MACD'], stockdata_df['MACDsignal'], stockdata_df['MACDhist']
