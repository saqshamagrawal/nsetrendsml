import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')

import datetime
import pandas as pd
import matplotlib
matplotlib.use('TkAgg')  # must be called before importing pyplot
import matplotlib.pyplot as plt
import gspread  # for accessing portfolio and watchlist from Google Spreadsheet
from oauth2client.service_account import ServiceAccountCredentials  # for authenticating access to Google Spreadsheet
import pkg_resources  # for properly accessing client_secret
from datapull import pullNSEdatafromQuandl  # hosts the function where Quandl is accessed to pull data
from datapull import pullNSEdatafromAlphaVantage  # hosts the function where Alpha Vantage is accessed to pull data
from technicals import gettechnicals
from features import getfeatures
import candlestick


# Access the watchlist

scope = ['https://spreadsheets.google.com/feeds']
path = '/secret/client_secret.json'  # downloaded from Google API Console and saved at this location

CLIENT_SECRET_FILE = pkg_resources.resource_filename(__name__, path)
credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRET_FILE, scope)  # gives error unless chdir

gc = gspread.authorize(credentials)
daily_execution = gc.open_by_url('https://docs.google.com/spreadsheets/d/1d4RnsjtGMxEN3qWuhBTNdy4iWFl7Y_242uGaG5j7Zyg/')
# "Daily Execution" sheet, shared with "readportfoliodata@stockportfolionsetrends.iam.gserviceaccount.com"
watchlist = daily_execution.worksheet('Watchlist').get_all_records(empty2zero=False, head=1, default_blank='')
watchlist = pd.DataFrame(watchlist)  # makes it a pandas DataFrame

# watchlist_largecap = watchlist[watchlist['Category'] > 'Large']
# watchlist_midcap = watchlist[watchlist['Market Cap'] < 10000]

# Set parameters

testconfiguration = {
    'averaging_period': 14,  # for calculating technicals like RSI, and rolling means
    'lookback_period': 14,  # lookback period for getting features (aggregates) over these number of periods
    'history': 3650,  # Calendar days, not trading days
    # The first few days (=averaging_period, but also few more in the lookback period) are not going to contribute
    'testname': datetime.datetime.now().strftime("test_%y_%m_%d"),
}

watchlist = watchlist[:50]  # For nifty50


# -------- Quandl Datapull --------

benchmarks = {
    # 'NSE/CNX_NIFTY_JUNIOR',
    # 'NSE/CNX_100_EQUAL_WEIGHT',
    'NSE/CNX_NIFTY'
}

# Setting dates for what data to pull
datapull_start_date = (datetime.datetime.today() - datetime.timedelta(testconfiguration['history'])).date()
datapull_end_date = datetime.datetime.today().date()
# always pulling latest data, assuming that all tests will be till the most recent info available

NSEdata = pullNSEdatafromQuandl(watchlist['Ticker'], benchmarks, datapull_start_date, datapull_end_date)


# -------- Alpha Vantage Datapull --------

# benchmarks not found in AV data

NSEdata, datapull_time = pullNSEdatafromAlphaVantage(watchlist['Ticker'])
# NSEdata is a dict of dataframes
# datapull_time is a datetime.datetime useful to judge how updated the data is
# use it like this:
# datetime.datetime.fromtimestamp(datapull_time)

# Or read from pickle
signalsdf = pd.read_pickle('NSEdata_AV_MAR15.pickle')


# -------- Testing NSEdata --------

# Testing the alpha vantage datapull

# Test 1 - How many keys pulled
len(NSEdata)
# 143 keys were pulled

# Test 2 - Which keys not pulled
for ticker in watchlist.Ticker:
    if ticker in NSEdata.keys():
        pass
    else:
        print(ticker)
# 2 not found
# MCDOWELL-N
# DALMIABHA

# Test 3 - Others with too little data (probably because lot of data):
for ticker in NSEdata.keys():
    a = len(NSEdata[ticker])
    b = (NSEdata[ticker].index.max() - NSEdata[ticker].index.min()).days
    x = a/b
    if (x < 0.65):  # typical value for this ratio is 0.6789, so anything below 0.65 means more than 5% data missing
        print(ticker, end="   ")
        print(x)
# NESTLEIND   0.4552657642786398
# WHIRLPOOL   0.4144932320897986
# Lot of zeros in above, but none are in nifty50
# HINDZINC   0.5262462859029383
# GAIL   0.6322396576319543
# PGHH   0.623473093430175
# MARICO   0.6315263908701855

# Test 4 - Data not available prior to 2013

for ticker in NSEdata.keys():
    if NSEdata[ticker].index.min() > datetime.datetime.strptime('2013-01-01','%Y-%m-%d'):
        print(ticker, end="  ")
        print(NSEdata[ticker].index.min().date())
#
#     LTI  2016-07-21
#     BANDHANBNK  2018-03-27
#     ENDURANCE  2016-10-19
#     NIACL  2017-11-13
#     HDFCAMC  2018-08-07
#     RBLBANK  2016-08-31
#     INDIGO  2015-11-10
#     ABCAPITAL  2017-09-01
#     SBILIFE  2017-10-03
#     ALKEM  2015-12-23
# BAJAJ-AUTO  2015-01-02
#     HDFCLIFE  2017-11-17
#     ICICIGI  2017-09-27
#     IBULHSGFIN  2013-07-23
#     FRETAIL  2016-08-29
#     PNBHOUSING  2016-11-07
#     DMART  2017-03-21
#     HAL  2018-04-02
#     ICICIPRULI  2016-09-29
#     GICRE  2017-10-25

# All available from their IPO dates (confirmed against google searches)
# Except BAJAJ-AUTO - seems to be missing 2008 to 2015 data

# Test 5 - Data not available for the last day (today)
for ticker in NSEdata.keys():
    if (NSEdata[ticker].index.max().date() < datetime.datetime.today().date()):
        print(ticker, end="  ")
        print(NSEdata[ticker].index.max().date())

# M&M  2019-03-13
# M&MFIN  2019-03-13
# L&TFH  2019-03-13

# Test 6 - Visualize a candlestick
fig, ax = plt.subplots(nrows=1,ncols=1)
candlestick.plot_candlestick(ax,NSEdata['INFY'])

# Preparing NSEdata for testconfiguration

datapull_start_date = (datetime.datetime.today() - datetime.timedelta(testconfiguration['history'])).date()
datapull_end_date = datetime.datetime.fromtimestamp(datapull_time) - datetime.timedelta(1)
# datapull_end_date = datapull_time.date() - datetime.timedelta(1)  # if datapull during market hours, than daily close updated till yesterday
# datapull_end_date = datetime.datetime.today() - datetime.timedelta(1)  # if datapull_time not available

# tickers_to_remove = ['BAJAJ-AUTO','NESTLEIND','WHIRLPOOL']  # based on tests
tickers_to_remove = []
NSEdata_prepared ={}
for ticker in NSEdata.keys():
    if ticker not in tickers_to_remove:
        NSEdata_prepared[ticker] = NSEdata[ticker][datapull_start_date:datapull_end_date]
        print(NSEdata_prepared[ticker].index.min())
len(NSEdata_prepared)

# create a benchmark of some sort (replace with nifty when available
# step 1 - confirm they are all of the same length
key_tickers = ['INFY','RELIANCE','TCS','HDFCBANK','HINDUNILVR','ITC','HDFC','INFY','MARUTI','SBIN','KOTAKBANK','ONGC']
for ticker in NSEdata_prepared.keys():
    if ticker in key_tickers:
        print(len(NSEdata_prepared[ticker]))  # all are same length

# step 2 - initialize benchmark
NSEdata_prepared['benchmark'] = pd.DataFrame(index=NSEdata_prepared['INFY'].index, columns=NSEdata_prepared['INFY'].keys())
NSEdata_prepared['benchmark'] = NSEdata_prepared['benchmark'].fillna(0)

# step 3 - combine the volumes to use as weights
for ticker in NSEdata_prepared.keys():
    if ticker in key_tickers:
        NSEdata_prepared['benchmark']['Total Trade Quantity'] += NSEdata_prepared[ticker]['Total Trade Quantity']

# step 4 - combine Prices using Volumes
for ticker in NSEdata_prepared.keys():
    if ticker in key_tickers:
        NSEdata_prepared['benchmark']['Open'] += NSEdata_prepared[ticker]['Open'] * NSEdata_prepared[ticker]['Total Trade Quantity']
        NSEdata_prepared['benchmark']['High'] += NSEdata_prepared[ticker]['High'] * NSEdata_prepared[ticker]['Total Trade Quantity']
        NSEdata_prepared['benchmark']['Low'] += NSEdata_prepared[ticker]['Low'] * NSEdata_prepared[ticker]['Total Trade Quantity']
        NSEdata_prepared['benchmark']['Close'] += NSEdata_prepared[ticker]['Close'] * NSEdata_prepared[ticker]['Total Trade Quantity']

# step 5 - normalize using total volume
NSEdata_prepared['benchmark']['Open'] = NSEdata_prepared['benchmark']['Open'] / NSEdata_prepared['benchmark']['Total Trade Quantity']
NSEdata_prepared['benchmark']['High'] = NSEdata_prepared['benchmark']['High'] / NSEdata_prepared['benchmark']['Total Trade Quantity']
NSEdata_prepared['benchmark']['Low'] = NSEdata_prepared['benchmark']['Low'] / NSEdata_prepared['benchmark']['Total Trade Quantity']
NSEdata_prepared['benchmark']['Close'] = NSEdata_prepared['benchmark']['Close'] / NSEdata_prepared['benchmark']['Total Trade Quantity']
NSEdata_prepared['benchmark']['Total Trade Quantity'] = NSEdata_prepared['benchmark']['Total Trade Quantity'] / NSEdata_prepared['benchmark']['Total Trade Quantity']

# step 6 - visualize the benchmark
fig, ax = plt.subplots(nrows=1,ncols=1)
candlestick.plot_candlestick(ax,NSEdata_prepared['benchmark'])



# -------- Technical Indicators --------

NSEdatawithtechnicals = {}
for ticker, stockdata in NSEdata_prepared.items():
    if len(stockdata) > 0:
        # NSEdatawithtechnicals[ticker] = gettechnicals(stockdata,testconfiguration['averaging_period'])
        NSEdatawithtechnicals[ticker] = gettechnicals(stockdata, 14)

print("Technical indicators added")

df = pd.concat(NSEdatawithtechnicals.values(),keys=NSEdatawithtechnicals.keys(),sort=False)
df.index.names = ['Ticker','Date']
df.to_pickle('NSEdata_50_APR7_withtechnicals.pickle')  # saving to file

df = pd.read_pickle('NSEdata_50_APR7_withtechnicals.pickle')  # reading from file

# to get back to NSEdata
NSEdatawithtechnicals = {}
for ticker in df.index.levels[0]:
    NSEdatawithtechnicals[ticker] = df.loc[ticker]

# -------------- Adding market breadth (can be of several types) ----------------

# technicals are required before this, because it contains log_daily_returns or other technicals to define stocks as bullish or bearish

# step 1 - initialize a dataframe of dates vs. tickers
advanced_declined_bool_fast = pd.DataFrame(index=NSEdatawithtechnicals['INFY'].index, columns=NSEdatawithtechnicals.keys())
advanced_declined_bool_slow = pd.DataFrame(index=NSEdatawithtechnicals['INFY'].index, columns=NSEdatawithtechnicals.keys())

# step 2 - populate each row with True and False for each day of advance vs. decline (uses df.update(Series.rename), sort of a left join
for ticker in NSEdatawithtechnicals.keys():
    # Define every ticker on every day as bullish or bearish
    # One day return is too "noisy", and to smoothen it, we use either MACD or RSI to define bullish or bearish
    # MACD provides various "timeframes" over which stock is bullish or bearish
    advanced_declined_bool_fast[ticker].update((NSEdatawithtechnicals[ticker]['MACD12269'] > NSEdatawithtechnicals[ticker]['MACD12269_signal']).rename(ticker, inplace=True))
    advanced_declined_bool_slow[ticker].update((NSEdatawithtechnicals[ticker]['MACD245218'] > NSEdatawithtechnicals[ticker]['MACD245218_signal']).rename(ticker, inplace=True))
    # RSI14 gives very similar breadth as MACD245218

declined_count_fast = (advanced_declined_bool_fast == False).sum(axis=1); advanced_count_fast = (advanced_declined_bool_fast == True).sum(axis=1)
declined_count_slow = (advanced_declined_bool_slow == False).sum(axis=1); advanced_count_slow = (advanced_declined_bool_slow == True).sum(axis=1)

# step 3 - define a method to combine advanced and declined counts to a usable metric
market_breadth_fast = (advanced_count_fast - declined_count_fast) / (advanced_count_fast + declined_count_fast)
market_breadth_slow = (advanced_count_slow - declined_count_slow) / (advanced_count_slow + declined_count_slow)


# -------- Features --------

NSEdatawithfeatures = {}
for ticker, technicaldata in NSEdatawithtechnicals.items():
    print("features extracted for ticker"+ticker)
    NSEdatawithfeatures[ticker] = getfeatures(technicaldata,{},market_breadth_fast,market_breadth_slow,14)

print("Features extracted")

# List of features
print(NSEdatawithfeatures['INFY'].keys())

signalsdf = pd.concat(NSEdatawithfeatures.values(),keys=NSEdatawithfeatures.keys(),sort=False)
signalsdf.index.names = ['Ticker','Date']

# For a specific strategy,
# insert "read strategy.json" here
# and apply strategy.entry

signalsdf.to_pickle('NSEdata_50_APR7_withfeatures.pickle')  # saving to file

# to get back to NSEdatawithfeatures
# NSEdatawithfeatures_fromdf = {}
# for ticker in signalsdf.index.levels[0]:
#     NSEdatawithfeatures_fromdf[ticker] = signalsdf.loc[ticker]


