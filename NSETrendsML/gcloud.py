# Trying out code to write image output to google cloud storage
# Not used otherwise

import pkg_resources  # for properly accessing client_secret
from google.cloud import storage  # installed "google-cloud-storage" python library for this
from oauth2client.service_account import ServiceAccountCredentials
# Deprecated, needs replacing - https://google-auth.readthedocs.io/en/latest/oauth2client-deprecation.html
import os


scope = ['https://spreadsheets.google.com/feeds']
path = '/secret/client_secret.json'  # downloaded from Google API Console and saved at this location

CLIENT_SECRET_FILE = pkg_resources.resource_filename(__name__, path)
credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRET_FILE, scope)  # gives error unless chdir


client = storage.Client(credentials=credentials, project='myproject')
bucket = client.get_bucket('mybucket')
blob = bucket.blob('myfile')
blob.upload_from_filename('myfile')



client = storage.Client()


# https://console.cloud.google.com/storage/browser/credibleinformatics.com/CredM_Images/
bucket = client.get_bucket('credibleinformatics.com')
# bucket = client.get_bucket('bucket-id-here')
# Then do other things...
blob = bucket.blob('CredM_Images/CredM_daily_chart.png')

filename = os.path.join('output', 'CredM_daily_chart.png')
blob.upload_from_filename(filename=filename)

blob = bucket.get_blob('CredM_Images/CredM_daily_chart.png')
print(blob.download_as_string())
blob.upload_from_string('New contents!')
blob2 = bucket.blob('remote/path/storage.txt')
blob2.upload_from_filename(filename='/local/path.txt')



