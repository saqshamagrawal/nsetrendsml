import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz  # To really use this, Graphviz must be installed separately on Local
from sklearn.feature_selection import RFE
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import pydotplus
from sklearn.externals.six import StringIO
from functions import printPerformance



# listing features by type
discrete_events = ['MACD12269_pos0cross', 'MACD12269_neg0cross', 'MACD12269_possignalcross', 'MACD12269_negsignalcross', 'MACD245218_pos0cross',
                   'MACD245218_neg0cross', 'MACD245218_possignalcross', 'MACD245218_negsignalcross', 'MACD6135_pos0cross', 'MACD6135_neg0cross',
                   'MACD6135_possignalcross', 'MACD6135_negsignalcross', 'MACD362_pos0cross', 'MACD362_neg0cross', 'MACD362_possignalcross',
                   'MACD362_negsignalcross', 'RSI_crossinto30', 'RSI_crossoutof30', 'RSI_crossinto70', 'RSI_crossoutof70', 'pricevolcorr_poscross',
                   'pricevolcorr_negcross', 'pricevolcorr_siginc', 'pricevolcorr_sigdec']
discrete_states = ['MACD12269_gt0', 'MACD12269_gtSignal', 'MACD245218_gt0', 'MACD245218_gtSignal', 'MACD6135_gt0', 'MACD6135_gtSignal', 'MACD362_gt0',
                   'MACD362_gtSignal', 'RSI_lt30', 'RSI_gt70', 'RSI_lt30_2weeksago', 'RSI_gt70_2weeksago', 'RSI_lt30_last7days', 'RSI_gt70_last7days',
                   'avgVol_rose_2weeksago', 'avgVol_rose_last7days', 'pricevolcorr_gt0', 'marketbreadthfast_gt0', 'marketbreadthslow_gt0',
                   'volatilityrose_2weeksago', 'volatilityrose_last7days', 'onedayreturn_gt0']
continuous = ['daily_volatility', 'pricevolcorr', 'avgVol_Volumevsavgvolume', 'avgVol_increase_last7days', 'pricevolcorr_delta', 'marketbreadthfast',
              'marketbreadthslow', 'volatilityincrease_last7days']

list_of_feature_lists = [discrete_events, discrete_states, continuous]

all_features = [item for sublist in list_of_feature_lists for item in sublist]

# features_to_model = [x for x in all_features if (x not in getFeaturesfromConditions(entryConditions))]
features_to_model = all_features

to_model = signalsdf.copy().reset_index()

# Fix missing features
# np.isnan(to_model).any() # is all false
for key in features_to_model:
    a = to_model[key].isnull().sum()
    if a > 0:
        print(key + " is missing " + str(a) + " out of " + str(len(to_model[key])) + " values")
# Sometimes early values can be inf
for key in features_to_model:
    a = len(to_model[to_model[key] == np.inf])
    if a > 0:
        print(key + " has inf values for " + str(a) + " out of " + str(len(to_model[key])) + " values")
to_model = to_model.replace([np.inf, -np.inf], np.nan)
# if not too many, drop the rows
to_model.dropna(inplace=True)  # drop rows with missing values

# here, outliers must be removed
# which depends on the exit criteria applied, so, for MAX1
len(to_model[to_model.signal_return > 0.2])
len(to_model[to_model.signal_return < -0.2])
to_model = to_model[(to_model.signal_return > -0.2) & (to_model.signal_return < 0.2)]

# for MAX10
to_model = to_model[(to_model.signal_return > -0.4) & (to_model.signal_return < 0.4)]

printPerformance(to_model)

X = to_model[features_to_model]; y = to_model['winloss']

from sklearn.ensemble import RandomForestClassifier
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
rf = RandomForestClassifier(n_estimators=100, criterion='gini', max_depth=5, min_samples_split=40, min_samples_leaf=20)
rf.fit(X_train, y_train)
y_pred = rf.predict_proba(X_test)

# To examine feature importances of the forest
print('Feature Importances')
print(pd.DataFrame(rf.feature_importances_, index=X_train.columns, columns=['importance']).sort_values('importance', ascending=False))

from sklearn.externals import joblib
filename = os.path.join('models', 'randomforest.model')
joblib.dump(rf,filename)

# Scoring random forest
scored_trades = to_model.replace([np.inf, -np.inf], np.nan)
scored_trades = scored_trades.dropna()
scored_trades['proba_rf'] = rf.predict_proba(scored_trades[all_features])[:, 1]

a = scored_trades.groupby(by=pd.qcut(scored_trades.proba_rf, 20))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss.mean())  # Does it rankorder winloss
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss2.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss5.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.signal_return.mean())  # Does it rankorder avg signal return
a.apply(printPerformance)

fig, ax = plt.subplots(nrows=1,ncols=1)
plt.hist(scored_trades['proba_rf'])  # are there outliers
fig, ax = plt.subplots(nrows=1,ncols=1)
plt.plot(a.groupby(by=pd.qcut(a.proba_rf, 100)).signal_return.mean().values)

# RF with short gives similar model outcomes

to_model_long = scored_trades[scored_trades.proba_rf > np.percentile(scored_trades.proba_rf,95)]; printPerformance(to_model_long)

long_original = subset_originals(to_model_long,20)
printPerformance(long_original)
# number of signals by month
monthly_trades_long = long_original.reset_index(level=['Ticker']).groupby(pd.Grouper(freq='M')).aggregate({'Ticker': {'Signals': 'count'}})
fig = plt.figure(); plt.plot(monthly_trades_long)


b = long_original.groupby(by=pd.qcut(long_original.proba_rf, 5))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(b.proba_rf.mean(),b.signal_return.mean())  # Does it rankorder avg signal return

# Check whether performance is reasonable with other exit criteria
x = long_original.copy()
x[performance_vars] = x.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MIN0','static2','static4'),axis=1)
printPerformance(x)

# linear regresion on signal return
from sklearn.linear_model import LinearRegression
X = to_model[features_to_model]; y = to_model['signal_return']
reg = LinearRegression()
# Recursive Feature Elimination
rfe = RFE(reg, 20)
rfe = rfe.fit(X, y.values.ravel())
print(rfe.support_)
print(rfe.ranking_)
# It suggests which features are useful
features_RFE = X.keys().values[rfe.support_].tolist()
print("features selected by RFE: ",len(features_RFE))
X = X[features_RFE]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
reg = LinearRegression().fit(X_train, y_train)
X_test['predicted_return'] = reg.predict(X_test[features_RFE])
X_test['signal_return'] = y_test
a = X_test.groupby(by=pd.qcut(X_test['predicted_return'], 100))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.predicted_return.mean(),a.signal_return.mean())

from sklearn.externals import joblib
filename = os.path.join('models', 'regression.model')
joblib.dump(reg,filename)


long_original['predicted_return'] = reg.predict(long_original[features_to_model])

a = long_original.groupby(by=pd.qcut(long_original.predicted_return, 10))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.predicted_return.mean(),a.signal_return.mean())



to_model_short = scored_trades[scored_trades.proba_rf < np.percentile(scored_trades.proba_rf,5)]; printPerformance(to_model_short)
from functions import subset_originals
short_original = subset_originals(to_model_short,20)
printPerformance(short_original)
# number of signals by month
monthly_trades_short = short_original.reset_index(level=['Ticker']).groupby(pd.Grouper(freq='M')).aggregate({'Ticker': {'Signals': 'count'}})
fig = plt.figure(); plt.plot(monthly_trades_short)

fig = plt.figure(); plt.plot(monthly_trades_short + monthly_trades_long)

print(monthly_trades_long+monthly_trades_short)

# Here we are confident that we have reasonably modeled winloss, it's persistent enough
# Though its apparent that the model is slightly different on each run, and may impact next steps


# let's try and do linear regression against signal return, after clipping very high and very low values

plt.hist(to_model_long.signal_return)