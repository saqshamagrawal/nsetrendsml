import os
os.chdir('/home/saksham/PycharmProjects/NSETrendsML/NSETrendsML')

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz  # To really use this, Graphviz must be installed separately on Local
from sklearn.feature_selection import RFE
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import pydotplus
from sklearn.externals.six import StringIO
from functions import printPerformance



# listing features by type
discrete_events = ['MACD12269_pos0cross', 'MACD12269_neg0cross', 'MACD12269_possignalcross', 'MACD12269_negsignalcross', 'MACD245218_pos0cross',
                   'MACD245218_neg0cross', 'MACD245218_possignalcross', 'MACD245218_negsignalcross', 'MACD6135_pos0cross', 'MACD6135_neg0cross',
                   'MACD6135_possignalcross', 'MACD6135_negsignalcross', 'MACD362_pos0cross', 'MACD362_neg0cross', 'MACD362_possignalcross',
                   'MACD362_negsignalcross', 'RSI_crossinto30', 'RSI_crossoutof30', 'RSI_crossinto70', 'RSI_crossoutof70', 'pricevolcorr_poscross',
                   'pricevolcorr_negcross', 'pricevolcorr_siginc', 'pricevolcorr_sigdec']
discrete_states = ['MACD12269_gt0', 'MACD12269_gtSignal', 'MACD245218_gt0', 'MACD245218_gtSignal', 'MACD6135_gt0', 'MACD6135_gtSignal', 'MACD362_gt0',
                   'MACD362_gtSignal', 'RSI_lt30', 'RSI_gt70', 'RSI_lt30_2weeksago', 'RSI_gt70_2weeksago', 'RSI_lt30_last7days', 'RSI_gt70_last7days',
                   'avgVol_rose_2weeksago', 'avgVol_rose_last7days', 'pricevolcorr_gt0', 'marketbreadthfast_gt0', 'marketbreadthslow_gt0',
                   'volatilityrose_2weeksago', 'volatilityrose_last7days', 'onedayreturn_gt0']
continuous = ['daily_volatility', 'pricevolcorr', 'avgVol_Volumevsavgvolume', 'avgVol_increase_last7days', 'pricevolcorr_delta', 'marketbreadthfast',
              'marketbreadthslow', 'volatilityincrease_last7days']

list_of_feature_lists = [discrete_events, discrete_states, continuous]

all_features = [item for sublist in list_of_feature_lists for item in sublist]

# features_to_model = [x for x in all_features if (x not in getFeaturesfromConditions(entryConditions))]
features_to_model = all_features

to_model = signalsdf.copy().reset_index()

# Fix missing features
# np.isnan(to_model).any() # is all false
for key in features_to_model:
    a = to_model[key].isnull().sum()
    if a > 0:
        print(key + " is missing " + str(a) + " out of " + str(len(to_model[key])) + " values")
# Sometimes early values can be inf
for key in features_to_model:
    a = len(to_model[to_model[key] == np.inf])
    if a > 0:
        print(key + " has inf values for " + str(a) + " out of " + str(len(to_model[key])) + " values")
to_model = to_model.replace([np.inf, -np.inf], np.nan)
# if not too many, drop the rows
to_model.dropna(inplace=True)  # drop rows with missing values

# here, outliers must be removed
# which depends on the exit criteria applied, so, for MAX1
len(to_model[to_model.signal_return > 0.2])
len(to_model[to_model.signal_return < -0.2])
to_model = to_model[(to_model.signal_return > -0.2) & (to_model.signal_return < 0.2)]

# for MAX10
to_model = to_model[(to_model.signal_return > -0.4) & (to_model.signal_return < 0.4)]

printPerformance(to_model)

X = to_model[features_to_model]; y = to_model['winloss']


# Basic understanding of the features vs. winloss
# for feature in discrete_states+discrete_events:
#     print(to_model[['winloss',feature]].groupby(feature).mean())

# A Random Forest

# A simple Decision Tree Classifier

# # for i in range(10,100,5):
# clf_gini = DecisionTreeClassifier(criterion="gini", random_state=10, max_depth=4, min_samples_leaf=40)
# # clf_entropy = DecisionTreeClassifier(criterion="entropy", random_state=10, max_depth=10, min_samples_leaf=400)
# clf_gini.fit(X_train, y_train)
# y_pred = clf_gini.predict(X_test)
# print(accuracy_score(y_test, y_pred) * 100)
# dot_data = StringIO()
# export_graphviz(clf_gini, out_file=dot_data, filled=True, feature_names=features_to_model)
# graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
# graph.write_png('tree.png') # had to install Graphviz on laptop

from sklearn.ensemble import RandomForestClassifier
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
rf = RandomForestClassifier(n_estimators=100, criterion='gini', max_depth=5, min_samples_split=40, min_samples_leaf=20)
rf.fit(X_train, y_train)
y_pred = rf.predict_proba(X_test)
# errors = abs(y_pred[:,1] - y_test)
# mean_abs_perc_error = 100 * errors / y_test

fig, ax = plt.subplots(nrows=1,ncols=1)
from sklearn import calibration
curve = calibration.calibration_curve(y_test, y_pred[:,1], normalize=False, n_bins=100)
plt.scatter(curve[1],curve[0]) # predicted on x axis, real on y axis, but not equal buckets

# To examine a tree:
tree_6 = rf.estimators_[6]
dot_data = StringIO()
export_graphviz(tree_6, out_file=dot_data, filled=True, feature_names=continuous)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png('tree.png')

# To examine feature importances of the forest
fi1 = pd.DataFrame(rf.feature_importances_, index=X_train.columns, columns=['importance']).sort_values('importance', ascending=False)

# Scoring
scored_trades = to_model.replace([np.inf, -np.inf], np.nan)
scored_trades = scored_trades.dropna()
scored_trades['proba_rf'] = rf.predict_proba(scored_trades[all_features])[:, 1]

a = scored_trades.groupby(by=pd.qcut(scored_trades.proba_rf, 20))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss.mean())  # Does it rankorder winloss
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss2.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.winloss5.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_rf.mean(),a.signal_return.mean())  # Does it rankorder avg signal return
a.apply(printPerformance)

fig, ax = plt.subplots(nrows=1,ncols=1)
plt.hist(scored_trades['proba_rf'])  # are there outliers
fig, ax = plt.subplots(nrows=1,ncols=1)
plt.plot(a.groupby(by=pd.qcut(a.proba_rf, 100)).signal_return.mean().values)


# to run one model after another, do below:
to_model_long = scored_trades[scored_trades.proba_rf > np.percentile(scored_trades.proba_rf,95)]; printPerformance(to_model_long)
to_model_short = scored_trades[scored_trades.proba_rf < np.percentile(scored_trades.proba_rf,5)]; printPerformance(to_model_short)

# Logistic Regression Classifier for winloss2 (Long)

X = to_model_long[features_to_model]; y = to_model_long['winloss5']

lr_long = LogisticRegression(class_weight='balanced')

# Recursive Feature Elimination
rfe = RFE(lr_long, 10)
rfe = rfe.fit(X, y.values.ravel())
print(rfe.support_)
print(rfe.ranking_)
# It suggests which features are useful
features_RFE = X.keys().values[rfe.support_].tolist()
print("features selected by RFE: ",len(features_RFE))

X = X[features_RFE]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

lr_long.fit(X_train, y_train)
y_pred = lr_long.predict(X_test)
print(confusion_matrix(y_test, y_pred, labels=None))
lr_long.score(X_test, y_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(lr_long.score(X_test, y_test)))
print(classification_report(y_test, y_pred))
sklearn.metrics.balanced_accuracy_score(y_test, y_pred)
print(lr_long.coef_)
# Print coeffs of features (do var signs make sense)
dict_feature_coefs = dict(zip(features_RFE, lr_long.coef_.tolist()[0]))
for a, b in dict_feature_coefs.items():
    print(a,b)

# View the Receiver Operation Characteristic (kind of like a Lorenz curve)
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

logit_roc_auc = roc_auc_score(y_test, lr_long.predict(X_test))
fpr, tpr, thresholds = roc_curve(y_test, lr_long.predict_proba(X_test)[:, 1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1], 'r--'); plt.xlim([0.0, 1.0]); plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate'); plt.ylabel('True Positive Rate'); plt.title('Receiver operating characteristic')
plt.legend(loc="lower right"); plt.savefig('Log_ROC'); plt.show()

# View the histogram of proba for test
a = lr_long.predict_proba(X_test)[:, 1]
fig, ax = plt.subplots(nrows=1,ncols=1)
plt.hist(a)

# Score using the LR model
scored_trades_long = to_model_long.dropna()
scored_trades_long['proba_lr'] = lr_long.predict_proba(scored_trades_long[features_RFE])[:, 1]

a = scored_trades_long.groupby(by=pd.qcut(scored_trades_long.proba_lr, 5))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss.mean())  # Does it rankorder winloss
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss2.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss5.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.signal_return.mean())  # Does it rankorder avg signal return
a.apply(printPerformance)






# Logistic Regression Classifier for winloss2 (Short)

# Need to again scoreExit, or to avoid, do below but "buy" and "sell" prices are wrong
# to_model_short = to_model_short.set_index(['Ticker', 'Date'])
# to_model_short['longshort'] = 'short'
# to_model_short[performance_vars] = to_model_short.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX1','None','None'),axis=1)
# to_model_short = to_model_short.reset_index()

to_model_short['longshort'] = 'short'
to_model_short['signal_return'] = -1 * to_model_short['signal_return']
to_model_short['winloss'] = np.where(to_model_short['signal_return'] > 0,1,0)
to_model_short['winloss2'] = np.where(to_model_short['signal_return'] > 0.02,1,0)
to_model_short['winloss5'] = np.where(to_model_short['signal_return'] > 0.05,1,0)
printPerformance(to_model_short)

X = to_model_short[features_to_model]; y = to_model_short['winloss2']

lr_short = LogisticRegression(class_weight='balanced')

# Recursive Feature Elimination
rfe = RFE(lr_short, 10)
rfe = rfe.fit(X, y.values.ravel())
print(rfe.support_)
print(rfe.ranking_)
# It suggests which features are useful
features_RFE = X.keys().values[rfe.support_].tolist()
print("features selected by RFE: ",len(features_RFE))

X = X[features_RFE]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

lr_short.fit(X_train, y_train)
y_pred = lr_short.predict(X_test)
print(confusion_matrix(y_test, y_pred, labels=None))
lr_short.score(X_test, y_test)
print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(lr_short.score(X_test, y_test)))
print(classification_report(y_test, y_pred))
sklearn.metrics.balanced_accuracy_score(y_test, y_pred)
print(lr_short.coef_)
# Print coeffs of features (do var signs make sense)
dict_feature_coefs = dict(zip(features_RFE, lr_short.coef_.tolist()[0]))
for a, b in dict_feature_coefs.items():
    print(a,b)

# View the Receiver Operation Characteristic (kind of like a Lorenz curve)
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

logit_roc_auc = roc_auc_score(y_test, lr_short.predict(X_test))
fpr, tpr, thresholds = roc_curve(y_test, lr_short.predict_proba(X_test)[:, 1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1], 'r--'); plt.xlim([0.0, 1.0]); plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate'); plt.ylabel('True Positive Rate'); plt.title('Receiver operating characteristic')
plt.legend(loc="lower right"); plt.show()
# plt.savefig('Log_ROC')

# View the histogram of proba for test
a = lr_short.predict_proba(X_test)[:, 1]
fig, ax = plt.subplots(nrows=1,ncols=1)
plt.hist(a)

# Score using the LR model
scored_trades_short = to_model_long.dropna()
scored_trades_short['proba_lr'] = lr_long.predict_proba(scored_trades_short[features_RFE])[:, 1]

a = scored_trades_short.groupby(by=pd.qcut(scored_trades_short.proba_lr, 5))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss.mean())  # Does it rankorder winloss
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss2.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss5.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.signal_return.mean())  # Does it rankorder avg signal return
a.apply(printPerformance)




# select high proba_lr for inspection

long_selected = scored_trades_long[scored_trades_long.proba_lr > 0.6]
short_selected = scored_trades_short[scored_trades_short.proba_lr > 0.6]

# Lot of duplicates

# remove duplicates

long_dedup = subset_originals(scored_trades_long,20)

a = long_dedup.groupby(by=pd.qcut(long_dedup.proba_lr, 2))
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss.mean())  # Does it rankorder winloss
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.winloss2.mean())  # Does it rankorder winloss2
fig, ax = plt.subplots(nrows=1,ncols=1); plt.scatter(a.proba_lr.mean(),a.signal_return.mean())  # Does it rankorder avg signal return
a.apply(printPerformance)










fig, ax = plt.subplots(nrows=1,ncols=1)
plt.hist(scored_trades_long['proba_lr'])  # are there outliers
# plot_random_signal(scored_trades,['proba'],scored_trades)
# for a given date
a = scored_trades.iloc[scored_trades.index.get_level_values('Date') == datetime.datetime(2019, 2, 11, 0, 0)][['proba','signal_return']]
# for a given ticker
a = scored_trades.iloc[scored_trades.index.get_level_values('Ticker') == 'ITC'][['proba_lr','signal_return']]
# for a proba cutoff
a =  scored_trades[scored_trades.proba > 0.8]


# Other trials
# scored_trades[performance_vars] = scored_trades.apply(scoreExit,args=(NSEdatawithfeatures,combine([[]],'|'),'MAX4','None','None'),axis=1)
# scored_trades = scored_trades[scored_trades.daily_volatility > 0.01]

# selected trades on proba
chosen_trades_long = scored_trades[scored_trades_long.proba_lr > 0.7]
chosen_trades_short = scored_trades[scored_trades_short.proba_lr < 0.3]
printPerformance(chosen_trades_long)
printPerformance(chosen_trades_short)



fig, ax = plt.subplots(nrows=1,ncols=1)
plt.scatter(scored_trades['proba_rf'],scored_trades['proba_lr'])

a.groupby(pd.qcut(a.proba_lr, 10)).apply(printPerformance)

# Double groupby!
a.groupby([pd.qcut(a.proba_lr, 10),pd.qcut(a.proba_rf, 10)]).signal_return.mean()
b = a[a.proba_lr > 0.54]
b = b[b.proba_rf > 0.54]
b = b.set_index(['Ticker', 'Date'])

get_monthly_performance(b)

# number of signals by month
trades1 = trades.reset_index(level=['Ticker']).groupby(pd.Grouper(freq='M')).aggregate({'Ticker': {'Signals': 'count'}})
plt.plot(trades1)

b = scored_trades_long

from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(b.proba_rf, b.proba_lr, b.signal_return, c='r', marker='o')

ax.set_xlabel('proba_rf')
ax.set_ylabel('proba_lr')
ax.set_zlabel('return')

plt.show()


## SOMETHING WRONG WITH MODEL AS MANY VALUES IN A SINGLE MONTH

# Earlier attempt at using seaborn for bivariate visualization

# Visualization - bug - TKinter or Agg crashes after a few plots (some installation problem maybe)
# sns.lineplot(signalsdf['Open'])
#
# sns.regplot(signalsdf['beta_sortof'],signalsdf['signal_return'])
# sns.pairplot(signalsdf[['beta_sortof','signal_return']])  # not exactly a bivariate
#
# sns.jointplot('beta_sortof','MACDlowerthansignal', data=signalsdf,
#                    marginal_kws=dict(bins=10, rug=True),
#                    annot_kws=dict(stat="r"),
#                    s=40, edgecolor="w", linewidth=1)
